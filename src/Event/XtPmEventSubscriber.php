<?php

/**
 * @file
 * Contains \Drupal\sxt_pm\Event\XtPmEventSubscriber.
 */

namespace Drupal\sxt_pm\Event;

use Drupal\slogxt\Event\SlogxtEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Drupal\Component\EventDispatcher\Event;

/**
 */
class XtPmEventSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  static function getSubscribedEvents() {
    //
    $events[SlogxtEvents::SLOGXT_DESCRIPTION_ROUTES][] = ['getDescriptionRoutes', 9999];

    return $events;
  }

  /**
   */
  public function getDescriptionRoutes(Event $event) {
    $data = $event->getData();
    if (!is_array($data['result']['description_routes'])) {
      $data['result']['description_routes'] = [];
    }
    $routes = [
        'sxt_pm.mainproject.edit',
        'sxt_pm.roleproject.edit',
    ];
    $data['result']['description_routes'] = array_merge($data['result']['description_routes'], $routes);
  }
}
