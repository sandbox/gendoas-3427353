<?php

/**
 * @file
 * Contains \Drupal\sxt_pm\Access\XtpmXtRolePermissions.
 */

namespace Drupal\sxt_pm\Access;

/**
 * Provides permissions for Xt-Roles.
 */
class XtpmXtRolePermissions {

  /**
   * Returns an array of Xtsi Xt-Role permissions.
   *
   * @return array
   *   The Xt-Role permissions.
   *   @see \Drupal\user\PermissionHandlerInterface::getPermissions()
   */
  public function getXtRolePermissions() {
    $perms = [
        'admin sxtrole-project' => [
            'title' => 'Administer Xt-Role project',
            'description' => 'Permission for each of the Xt-Role project actions (new, edit, delete).',
        ],
    ];

    return $perms;
  }
}
