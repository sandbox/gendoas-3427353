<?php

/**
 * @file
 * Contains \Drupal\sxt_pm\Handler\admin\XtPmAutocompleteController
 */

namespace Drupal\sxt_pm\Handler\admin;

use Drupal\Component\Utility\Tags;
use Drupal\Component\Utility\Crypt;
use Drupal\Core\Site\Settings;
use Drupal\user\Entity\Role;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Drupal\system\Controller\EntityAutocompleteController;

/**
 */
class XtPmAutocompleteController extends EntityAutocompleteController {

  /**
   * {@inheritdoc}
   */
  public function handleAutocomplete(Request $request, $target_type, $selection_handler, $selection_settings_key) {
    $matches = [];
    $role_id = $request->get('base_entity_id');
    $role = Role::load($role_id);
    $project_id = $role ? (integer) $role->get('sxt_pm_project') : FALSE;
    if (!$project_id) {
      return $matches;
    }

    // Get the typed string from the URL, if it exists.
    if ($input = $request->query->get('q')) {
      $tag_list = Tags::explode($input);
      $typed_string = !empty($tag_list) ? mb_strtolower(array_pop($tag_list)) : '';

      // Selection settings are passed in as a hashed key of a serialized array
      // stored in the key/value store.
      $selection_settings = $this->keyValue->get($selection_settings_key, FALSE);
      if ($selection_settings !== FALSE) {
        $data = serialize($selection_settings) . $target_type . $role_id . $project_id . $selection_handler;
        $selection_settings_hash = Crypt::hmacBase64($data, Settings::getHashSalt());
        if (!hash_equals($selection_settings_hash, $selection_settings_key)) {
          // Disallow access when the selection settings hash does not match the
          // passed-in key.
          throw new AccessDeniedHttpException('Invalid selection settings key.');
        }
      } else {
        // Disallow access when the selection settings key is not found in the
        // key/value store.
        throw new AccessDeniedHttpException();
      }

      $matches = $this->matcher->getMatches($target_type, $selection_handler, $selection_settings, $typed_string);

    }

    return new JsonResponse($matches);
  }
}
