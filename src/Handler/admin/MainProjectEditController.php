<?php

/**
 * @file
 * Contains \Drupal\sxt_pm\Handler\admin\MainProjectEditController
 */

namespace Drupal\sxt_pm\Handler\admin;

use Drupal\sxt_pm\SlogXtPm;
use Drupal\slogxt\SlogXt;
use Drupal\pm_status\Entity\PmStatus;
use Drupal\pm_priority\Entity\PmPriority;
use Drupal\pm_board\Entity\PmBoardColumn;
use Drupal\node\Entity\Node;
use Drupal\node\Controller\NodePreviewController;
use Drupal\Core\Form\FormStateInterface;
use Drupal\slogxt\Controller\XtEditControllerBase;
use Drupal\Core\Security\TrustedCallbackInterface;
use Drupal\slogxt\Form\XtPreviewFormTrait;

/**
 */
class MainProjectEditController extends XtEditControllerBase implements TrustedCallbackInterface {

  use XtPreviewFormTrait;

  protected $request = NULL;
  protected $action = NULL;
  protected $has_action = FALSE;
  protected $component = NULL;
  protected $has_component = FALSE;
  protected $component_labels = [];
  protected $pm_type = NULL;
  protected $has_pm_type = FALSE;
  protected $type_entity = NULL;
  protected $entity_id = NULL;
  protected $entity = NULL;
  protected $has_entity = FALSE;
  protected $is_for_entity = FALSE;
  protected $is_board_column = FALSE;
  protected $is_role_pmadmin = FALSE;
  //
  protected $err_msg = FALSE;
  protected $on_create_new = FALSE;
  protected $on_view = FALSE;
  protected $on_edit = FALSE;
  protected $on_editcolor = FALSE;
  protected $on_delete = FALSE;
  protected $on_select_component = FALSE;
  protected $on_select_entity = FALSE;
  protected $on_select_action = FALSE;
  protected $on_select_type = FALSE;

  /**
   */
  protected function isOnSelect() {
    return ($this->on_select_component || $this->on_select_type   //
            || $this->on_select_entity || $this->on_select_action);
  }

  protected function isOnCreate() {
    return ($this->on_create_new && !$this->on_select_type);
  }

  /**
   */
  protected function isOnEdit() {
    return ($this->on_edit || $this->on_editcolor);
  }

  /**
   */
  protected function getFormTitle() {
    $title = t('????');
    if ($this->component) {
      $component_label = (string) $this->component_labels[$this->component] ?? '???';
    }

    if ($this->on_select_component) {
      $title = t('Select component');
    } elseif ($this->on_select_entity) {
      $title = t('@comp: Select entity', ['@comp' => $component_label]);
    } elseif ($this->on_select_action) {
      $title = t('@comp: Select action', ['@comp' => $component_label]);
    } elseif ($this->on_create_new) {
      if ($this->on_select_type) {
        $title = t('Create new: @select', ['@select' => t('Select type')]);
      } else {
        $title = t('Create new: %comp', ['%comp' => $component_label]);
      }
    } elseif ($this->has_entity) {
      $args = [
          '%label' => $this->entity->label(),
          '@comp' => $component_label,
          '@type' => !$this->is_for_entity ? ' ' . t('Type') : '',
      ];
      if ($this->on_view) {
        $title = t('View @comp@type: %label', $args);
      } elseif ($this->isOnEdit()) {
        $title = t('Edit @comp@type: %label', $args);
      } elseif ($this->on_delete) {
        $title = t('Delete @comp@type: %label', $args);
      }
    }

    return $title;
  }

  protected function getSubmitLabel() {
    $label = t('Next');
    $on_finalize = $this->isOnPreview || ($this->component === 'pm_board_column');
    if ($this->isOnCreate()) {
      $label = $on_finalize ? t('Create') : t('Preview');
    } elseif ($this->isOnEdit()) {
      $label = $on_finalize ? t('Update') : t('Preview');
    } elseif ($this->on_delete) {
      $label = t('Delete');
    }

    return $label;
  }

  /**
   * Implements \Drupal\slogxt\Controller\AjaxFormControllerBase::getFormObjectArg();
   */
  protected function getFormObjectArg() {
    $this->request = $request = \Drupal::request();
    $this->component = $request->get('component');
    $this->has_component = $has_component = ($this->component !== '{component}');
    $this->entity_id = $request->get('entity_id');
    $this->has_entity = $has_entity = ($this->entity_id !== '{entity_id}');
    $this->pm_type = $request->get('pm_type');
    $this->has_pm_type = $has_pm_type = ($this->pm_type !== '{pm_type}');
    $this->action = $request->get('action');
    $this->has_action = $has_action = ($this->action !== '{action}');
    $this->on_create_new = $has_action && ($this->action === 'createnew');
    $this->on_view = $has_action && ($this->action === 'view');
    $this->on_edit = $has_action && ($this->action === 'edit');
    $this->on_editcolor = $has_action && ($this->action === 'editcolor');
    $this->on_delete = $has_action && ($this->action === 'delete');
    $comp_labels = SlogXtPm::getComponentLabels();
    $comp_labels['pm_project'] = t('Project');
    $type_labels = SlogXtPm::getPmTypeLabels();
    $this->component_labels = array_merge($comp_labels, $type_labels);
    if ($this->has_component) {
      $entity_keys = ['pm_status', 'pm_priority', '_pm_board_column_'];
      $this->is_for_entity = in_array($this->component, $entity_keys);
      $this->component = trim($this->component, '_');
      $request->attributes->set('component', $this->component);
      $this->is_board_column = $this->is_for_entity && ($this->component === 'pm_board_column');
    }

    if ($has_component && $has_entity) {
      if ($this->is_for_entity) {
        if ($this->component === 'pm_status') {
          $this->entity = SlogXtPm::getStatusEntity($this->entity_id);
        } elseif ($this->component === 'pm_priority') {
          $this->entity = SlogXtPm::getPriorityEntity($this->entity_id);
        } else {  // pm_board_column
          $this->entity = SlogXtPm::getBoardColumnEntity($this->entity_id);
        }
      } else {
        $this->entity = SlogXtPm::loadTypeEntity($this->component, $this->entity_id);
      }

      if (!$this->entity) {
        $args = [
            '@comp' => $this->component,
            '@eid' => $this->entity_id,
        ];
        $msg = t('Missing type entity (@comp: @eid)', $args);
        return SlogXt::arrangeUrgentMessageForm($msg, 'error');
      }
    }

    //
    if (!$has_action) {
      if (!$has_component) {
        $this->on_select_component = TRUE;
        $request->attributes->set('forType', TRUE);
        return '\Drupal\sxt_pm\Form\SelectPmComponentPlusForm';
      } elseif (!$has_entity) {
        $this->on_select_entity = TRUE;
        if ($this->is_for_entity) {
          $is_pm_pmadmin = \Drupal::currentUser()->hasPermission('administer xtpm components');
          $request->attributes->set('isRolePmAdmin', $is_pm_pmadmin);
          $request->attributes->set('forConfigEntity', TRUE);
          return '\Drupal\sxt_pm\Form\SelectPmEntityPlusForm';
        } else {
          $request->attributes->set('pmEntityTypeId', $this->component);
          return '\Drupal\sxt_pm\Form\SelectPmEntityTypePlusForm';
        }
      } elseif ($this->component === 'pm_board_column' && !$this->is_for_entity) {
        $msg = t('Editing not supported here.');
        return SlogXt::arrangeUrgentMessageForm($msg, 'warning');
      }
      //
      $this->on_select_action = TRUE;
      $request->attributes->set('isForEntity', $this->is_for_entity);
      $request->attributes->set('pmTypeEntity', $this->entity);
      return '\Drupal\sxt_pm\Form\SelectPmTypeActionForm';
    } elseif (!$has_entity && $has_component && $this->on_create_new) {
      if ($this->is_board_column && !$has_pm_type) {
        $this->on_select_type = TRUE;
        $types = SlogXtPm::getComponentBundles($this->component);
        if (count($types) === 1) {
          $this->on_select_type = FALSE;
          $this->pm_type = reset(array_keys($types));
          $this->has_pm_type = TRUE;
        } elseif (count($types) > 1) {
          $request->attributes->set('pmEntityTypeId', $this->component);
          $request->attributes->set('pathReplaceKey', '{pm_type}');
          return '\Drupal\sxt_pm\Form\SelectPmTypeForm';
        }
      }

      // 
      return $this->getEditFormObjectArg($this->component, 'add');
    } elseif ($has_entity && $this->on_view) {
      return 'Drupal\slogxt\Form\XtEmptyInfoForm';
    } elseif ($has_entity && $this->isOnEdit()) {
      return $this->getEditFormObjectArg($this->component, 'edit');
    } elseif ($has_entity && $this->on_delete) {
      if (!SlogXtPm::canDeleteTypeEntity($this->component, $this->entity)) {
        $msg = t('Deletion not allowed, component is used.');
        return SlogXt::arrangeUrgentMessageForm($msg, 'warning');
      }

      //
      $entity_type_id = $this->entity->getEntityTypeId();
      $form_arg = "$entity_type_id.delete";
      return $this->getEntityFormObject($form_arg, $this->entity);
    }

    // 
    return SlogXt::arrangeUrgentUnexpectedErrorForm();
  }

  /**
   */
  protected function getEditFormObjectArg($component, $opt) {
    if ($opt === 'add') {
      if ($this->is_for_entity) {
        if ($this->component === 'pm_status') {
          $this->entity = PmStatus::create([
                      'format' => 'xtitems_text',
                      'color' => NULL,
                      'status' => 1,
          ]);
        } elseif ($this->component === 'pm_priority') {
          $this->entity = PmPriority::create([
                      'format' => 'xtitems_text',
                      'color' => NULL,
                      'status' => 1,
          ]);
        } else {
          $this->entity = PmBoardColumn::create([
                      'bundle' => $this->pm_type,
          ]);
        }
      } else {
        $this->entity = SlogXtPm::createTypeEntity($component);
      }
    }

    //
    if ($this->is_for_entity) {
      $request = \Drupal::request();
      $request->attributes->set('pmTypeEntity', $this->entity);
      $request->attributes->set('editAction', $this->action);
      $request->attributes->set('editOpt', $opt);
      return '\Drupal\sxt_pm\Form\XtPmTypeEntityForm';
    } else {
      $entity_type_id = $this->entity->getEntityTypeId();
      $form_arg = "$entity_type_id.$opt";
      return $this->getEntityFormObject($form_arg, $this->entity);
    }
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::buildContentResult();
   */
  protected function buildContentResult(&$form, FormStateInterface $form_state) {
    $slogxtData = &$form_state->get('slogxtData');
    $slogxtData['showDonBtn'] = TRUE;
    if ($this->isOnSelect()) {
      $slogxtData['runCommand'] = 'radios';
      if ($this->component === 'pm_board_column' && !$this->is_for_entity) {
        $slogxtData['isLastPage'] = TRUE;
      }
    } elseif ($this->has_entity && $this->on_view) {
      $this->prepareViewData();
      $this->prepareDialogTmpPreview($slogxtData, $this->header, $this->content);
      return parent::_doBuildDialogViewContentResult($form, $form_state);
    } else {
      $slogxtData['runCommand'] = 'wizardFormEdit';
      $op = $this->request->get('op', FALSE);
      $op_save = ($op === (string) t('Save'));
      if ($op_save && !$form_state->hasAnyErrors()) {
        $slogxtData['wizardFinished'] = TRUE;
        $slogxtData['onWizardFinished'] = $this->getOnWizardFinished();
      } else {
        $slogxtData['wizardFinalize'] = TRUE;
      }
    }

    //
    return parent::buildContentResult($form, $form_state);
  }

  /**
   */
  protected function prepareViewData() {
    list($node_type, $field_name) = SlogXt::getDescriptionNodeKeys();
    $node = Node::create([
                'type' => $node_type,
                'title' => '',
                'status' => 1,
    ]);
    //
    $this->header = $this->entity->label();
    $description = (string) $this->entity->get('description')->value;
    $node->set($field_name, $description);
    $node_preview_controller = NodePreviewController::create(\Drupal::getContainer());
    $build = $node_preview_controller->view($node);
    $build['#prefix'] = '<div class="preview">';
    $build['#suffix'] = '</div>';
    $build['#pre_render'][] = [static::class, 'preRenderPreview'];
    $this->content = (string) \Drupal::service('renderer')->renderRoot($build);
  }

  /**
   */
  public static function trustedCallbacks() {
    return ['preRenderPreview'];
  }

  /**
   */
  protected function preRenderForm(&$form, FormStateInterface $form_state) {
    parent::preRenderForm($form, $form_state);
    if (!empty($form['actions']['submit'])) {
      $form['actions']['submit']['#value'] = t('Save');
      unset($form['actions']['delete']);
      unset($form['actions']['cancel']);
    }

    if ($this->on_delete) {
      $msg = (string) t('You are about to delete %name', ['%name' => $this->entity->label()]);
      $msg .= '<hr />' . t('WARNING: this action cannot be undone.');
      $form['description']['#markup'] = SlogXt::htmlMessage($msg, 'warning');
    }
  }

  /**
   */
  protected function getOnWizardFinished() {
    $steps = 1;
    if ($this->isOnCreate()) {
      $types = SlogXtPm::getComponentBundles($this->component);
      if (count($types) > 1) {
        $steps = 2;
      }
    } elseif ($this->on_edit || $this->on_delete) {
      $steps = 2;
    }
    return $this->commandRebuildPreviousPage('slogxt::finishedIdleFunc', $steps);
  }
}
