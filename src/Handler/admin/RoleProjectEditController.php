<?php

/**
 * @file
 * Contains \Drupal\sxt_pm\Handler\admin\RoleProjectEditController
 */

namespace Drupal\sxt_pm\Handler\admin;

use Drupal\sxt_pm\SlogXtPm;
use Drupal\pm_project\Entity\PmProject;
use Drupal\slogxt\SlogXt;
use Drupal\sxt_group\SxtGroup;
use Drupal\user\Entity\Role;
use Drupal\node\Entity\Node;
use Drupal\node\Controller\NodePreviewController;
use Drupal\Core\Render\Element;
use Drupal\Component\Utility\Crypt;
use Drupal\Core\Site\Settings;
use Drupal\Core\Form\FormStateInterface;
use Drupal\slogxt\Controller\XtEditControllerBase;
use Drupal\Core\Security\TrustedCallbackInterface;
use Drupal\slogxt\Form\XtPreviewFormTrait;

/**
 */
class RoleProjectEditController extends XtEditControllerBase implements TrustedCallbackInterface {

  use XtPreviewFormTrait;

  protected $request = NULL;
  protected $role = NULL;
  protected $is_role_pmadmin = FALSE;
  protected $project = NULL;
  protected $project_id = NULL;
  protected $pm_type = NULL;
  protected $has_pm_type = FALSE;
  protected $action = NULL;
  protected $has_action = FALSE;
  protected $component = NULL;
  protected $has_component = FALSE;
  protected $pm_components = [];
  protected $is_project_entity = FALSE;
  protected $is_board_column = FALSE;
  protected $entity_id = NULL;
  protected $entity = NULL;
  protected $has_entity = FALSE;
  //
  protected $err_msg = FALSE;
  protected $on_create_project = FALSE;
  protected $on_create_entity = FALSE;
  protected $on_select_type = FALSE;
  protected $on_select_component = FALSE;
  protected $on_select_entity = FALSE;
  protected $on_select_action = FALSE;

  protected function isOnView() {
    return ($this->action === 'pmview');
  }

  protected function isOnSelect() {
    return ($this->on_select_type   //
            || $this->on_select_component || $this->on_select_entity  //
            || $this->on_select_action);
  }

  protected function isOnCreate() {
    return ($this->on_create_project || $this->on_create_entity);
  }

  protected function isOnEditProject() {
    if ($this->is_project_entity) {
      $actions = ['edit', 'editstatus', 'editdate'];
      return in_array($this->action, $actions);
    }

    return FALSE;
  }

  protected function isOnEditEntity() {
    if ($this->has_entity && $this->has_action) {
      $actions = ['edit', 'editstatus', 'editdate', 'edittask', 'editassignee', 'editlist'];
      return in_array($this->action, $actions);
    } elseif ($this->has_component && !$this->is_project_entity) {
      return ($this->action === 'createnew');
    }

    return FALSE;
  }

  protected function isOnDelete() {
    return ($this->action === 'pmdelete');
  }

  /**
   */
  protected function getFormTitle() {
    $title = t('????');
    $component_label = (string) $this->pm_components[$this->component] ?? '???';
    if ($this->on_create_project) {
      $title = t('Create project');
      if ($this->on_select_type) {
        $title .= ': ' . t('Select type');
      } elseif ($this->isOnPreview) {
        $title .= ': ' . t('Preview');
      }
    } elseif ($this->on_create_entity) {
      $title = t('Create new component');
      if ($this->on_select_type) {
        $title .= ': ' . t('Select type');
      } elseif ($this->isOnPreview) {
        $title .= ': ' . t('Preview');
      } else {
        $title .= ': ' . t('%type', ['%type' => $component_label]);
      }
    } elseif ($this->on_select_component) {
      $title = t('Select component');
    } elseif ($this->on_select_entity) {
      $title = t('Select entity');
    } elseif ($this->on_select_action) {
      $title = t('Select action');
    } elseif ($this->action === 'edit') {
      if ($this->entity) {
        $args = [
            '@ecomp' => $component_label,
            '%elabel' => $this->entity->label()
        ];
      } else {
        $args = [
            '@ecomp' => t('Project'),
            '%elabel' => $this->project->label()
        ];
      }
      if ($this->isOnPreview) {
        $title = t('Edit @ecomp: Preview', $args);
      } else {
        $title = t('Edit @ecomp: %elabel', $args);
      }
    } elseif ($this->isOnView()) {
      if ($this->entity) {
        $args = [
            '@ecomp' => $component_label,
            '%elabel' => $this->entity->label()
        ];
      } else {
        $args = [
            '@ecomp' => t('Project'),
            '%elabel' => $this->project->label()
        ];
      }
      $title = t('View @ecomp: %elabel', $args);
    } elseif ($this->isOnEditProject()) {
      $action_labels = SlogXtPm::getEditActionLabels();
      $args = [
          '@eaction' => $action_labels[$this->action] ?? '??',
          '%elabel' => $this->project->label(),
      ];
      $title = t('Project @eaction: %elabel', $args);
    } elseif ($this->isOnEditEntity()) {
      $action_labels = SlogXtPm::getEditActionLabels();
      $args = [
          '@eaction' => $action_labels[$this->action] ?? '??',
          '%elabel' => $this->entity->label(),
      ];
      $title = t('@eaction: %elabel', $args);
    } elseif ($this->isOnDelete()) {
      $args = [
          '@ecomp' => $component_label,
          '%elabel' => $this->entity->label()
      ];
      $title = t('Delete @ecomp: %elabel', $args);
    }

    return $title;
  }

  protected function getSubmitLabel() {
    $label = t('Next');
    $on_finalize = $this->is_board_column || $this->isOnPreview;
    if ($this->isOnCreate()) {
      $label = $on_finalize ? t('Create') : t('Preview');
    } elseif ($this->action === 'edit') {
      $label = $on_finalize ? t('Update') : t('Preview');
    } elseif ($this->isOnEditProject() || $this->isOnEditEntity()) {
      $label = t('Save');
    } elseif ($this->isOnDelete()) {
      $label = t('Delete');
    }
    return $label;
  }

  /**
   * 
   * @return boolean
   */
  protected function initVars() {
//'path' => "/$baseSearchPath/pmedit/$base_entity_id
//            /{project_id}/{component}/{pm_type}/{action}/{entity_id}",
    $this->request = $request = \Drupal::request();
    $this->is_role_pmadmin = SxtGroup::hasPermission('admin sxtrole-project');
    $role_id = $request->get('base_entity_id');
    $this->role = $role = Role::load($role_id);
    $request->attributes->set('projectRole', $role);
    $project_id = (integer) $request->get('project_id');
    $this->action = $request->get('action');
    $this->has_action = $has_action = ($this->action !== '{action}');
    $this->pm_type = $request->get('pm_type');
    $this->has_pm_type = $has_pm_type = ($this->pm_type !== '{pm_type}');
    $this->component = $request->get('component');
    $this->has_component = $has_component = ($this->component !== '{component}');
    if ($this->has_component) {
      $this->is_project_entity = ($this->component === 'pm_project');
      $this->is_board_column = ($this->component === 'pm_board_column');
    }
    $this->entity_id = $request->get('entity_id');
    $this->has_entity = $has_entity = ($this->entity_id !== '{entity_id}');
    $this->pm_components = SlogXtPm::getComponentLabels();

    //
    $role_project_id = (integer) $role->get('sxt_pm_project');
    if (!$project_id && $role_project_id) {
      $project_id = $role_project_id;
    }

    //
    if ($project_id) {
      $role_project_id = (integer) $role->get('sxt_pm_project');
      if ($project_id !== $role_project_id) {
        $args = [
            '@pid' => $project_id,
            '@rpid' => $role_project_id,
        ];
        $this->err_msg = t('Project mismatch (@pid !== @rpid)', $args);
        SlogXtPm::logger()->error($this->err_msg);
        return FALSE;
      }

      $this->project = PmProject::load($project_id);
      if (!$this->project) {
        $role->set('sxt_pm_project', '')->save();
        $args = [
            '@pid' => $project_id,
            '@role' => $role->id(),
        ];
        $this->err_msg = t('Project not found (pid=@pid). Entry was deleted for the role @role.', $args);
        SlogXtPm::logger()->error($this->err_msg);
        return FALSE;
      }
    }

    // create new project
    if (!$this->project) {
      $this->on_create_project = TRUE;
      $this->on_select_type = !$has_pm_type;
      if ($this->on_select_type) {
        $types = SlogXtPm::getComponentBundles('pm_project');
        if (count($types) === 1) {
          $this->on_select_type = FALSE;
          $this->pm_type = reset(array_keys($types));
          $this->has_pm_type = TRUE;
        }
      }
      return TRUE;
    }

    // has project
    $this->project_id = $project_id;
    $request->attributes->set('project_id', $project_id);
    $request->attributes->set('pmProject', $this->project);
    $request->attributes->set('isRolePmAdmin', $this->is_role_pmadmin);

    // 
    if ($has_component && !$has_action && !$this->is_role_pmadmin) {
      if ($this->is_project_entity || $has_entity) {
        $this->action = 'pmview';
        $this->has_action = $has_action = TRUE;
        $request->attributes->set('action', $this->action);
      }
    }

    // 
    if (!$has_component && !$has_action) {
      $this->on_select_component = TRUE;
      return TRUE;
    } elseif ($this->is_project_entity) {
      $this->on_select_action = !$has_action;
      return TRUE;
    } elseif ($has_component && !$has_entity && !$has_action) {
      $this->on_select_entity = TRUE;
      return TRUE;
    } elseif ($has_component && !$has_entity && $has_action) {
      $this->on_create_entity = TRUE;
      $this->on_select_type = (!$has_pm_type && !$this->is_project_entity);
      if ($this->on_select_type) {
        $types = SlogXtPm::getComponentBundles($this->component);
        if (count($types) === 1) {
          $this->on_select_type = FALSE;
          $this->pm_type = reset(array_keys($types));
          $this->has_pm_type = TRUE;
        }
      }
      return TRUE;
    } elseif ($this->has_entity) {
      $entities = SlogXtPm::getComponentEntities($this->component, $project_id);
      $this->entity = $entities[$this->entity_id] ?? FALSE;
      if (!$this->entity) {
        $this->err_msg = t('Missing entity (@pid !== @rpid)', $args);
        SlogXtPm::logger()->error($this->err_msg);
        return FALSE;
      }
    }

    //
    $this->on_select_action = !$has_action;
    return TRUE;
  }

  /**
   * Implements \Drupal\slogxt\Controller\AjaxFormControllerBase::getFormObjectArg();
   */
  protected function getFormObjectArg() {
    if (!$this->initVars()) {
      $msg = !empty($this->err_msg) ? $this->err_msg : t('Init error occured.');
      return SlogXt::arrangeUrgentMessageForm($msg, 'error');
    }

    //
    $request = $this->request;
    if ($this->on_create_project) {
      $request->attributes->set('onCreateProject', TRUE);
      if ($this->on_select_type) {
        $request->attributes->set('pmEntityTypeId', 'pm_project');
        $request->attributes->set('pathReplaceKey', '{pm_type}');
        return '\Drupal\sxt_pm\Form\SelectPmTypeForm';
      }
      //
      $request->attributes->set('editAction', 'createnew');
      $request->attributes->set('projectTypeId', $this->pm_type);
      return '\Drupal\sxt_pm\Form\XtPmProjectForm';
    } elseif ($this->isOnView()) {
      return 'Drupal\slogxt\Form\XtEmptyInfoForm';
    } elseif ($this->on_select_component) {
      // has project, first selection
      return '\Drupal\sxt_pm\Form\SelectPmComponentPlusForm';
    } elseif ($this->isOnEditProject()) {
      $request->attributes->set('editAction', $this->action);
      return '\Drupal\sxt_pm\Form\XtPmProjectForm';
    } elseif ($this->on_select_entity) {
      return '\Drupal\sxt_pm\Form\SelectPmEntityPlusForm';
    } elseif ($this->on_select_action) {
      if ($this->is_project_entity) {
        $request->attributes->set('pmEntity', $this->entity);
        return '\Drupal\sxt_pm\Form\SelectPmProjectActionForm';
      } elseif ($this->has_entity) {
        $request->attributes->set('pmEntity', $this->entity);
        return '\Drupal\sxt_pm\Form\SelectPmEntityActionForm';
      }
    } elseif ($this->isOnEditEntity()) {
      $request->attributes->set('editAction', $this->action);
      $request->attributes->set('entityTypeId', $this->pm_type);
      if ($this->on_create_entity && $this->on_select_type) {
        $request->attributes->set('pmEntityTypeId', $this->component);
        $request->attributes->set('pathReplaceKey', '{pm_type}');
        return '\Drupal\sxt_pm\Form\SelectPmTypeForm';
      }

      //
      $request->attributes->set('pmEntity', $this->entity);
      return '\Drupal\sxt_pm\Form\XtPmEntityForm';
    } elseif ($this->isOnDelete()) {
      $entity_type_id = $this->entity->getEntityTypeId();
      $form_arg = "$entity_type_id.delete";
      return $this->getEntityFormObject($form_arg, $this->entity);
    }

    return SlogXt::arrangeUrgentUnexpectedErrorForm();
  }

  /**
   */
  protected function postBuildForm(&$form, FormStateInterface $form_state) {
    if ($this->isOnEditEntity()) {
      $list_labels = SlogXtPm::getComponentListLabels();
      $field_names = array_keys(SlogXtPm::getComponentListLabels());
      $field_names = array_merge($field_names, ['pm_task', 'pm_assignee', 'pm_reviewer']);
      foreach ($field_names as $field_name) {
        if (!empty($form[$field_name]['widget'])) {
          $this->prepareAutocomplete($form[$field_name]['widget'], $form_state);
        }
      }
    }
  }

  /**
   */
  protected function prepareAutocomplete(array &$widget, FormStateInterface &$form_state) {
    foreach (Element::children($widget) as $delta) {
      $element = & $widget[$delta]['target_id'];
      if ($element['#type'] !== 'entity_autocomplete' // 
              || empty($element['#autocomplete_route_name'])) {
        break;
      }

      //
      $element['#autocomplete_route_name'] = 'sxt_pm.entity_autocomplete';
      $element['#role_id'] = $this->role->id();
      $element['#project_id'] = $this->project_id;
      $element['#processed'] = FALSE;
      //
      $selection_settings = $element['#selection_settings'] ?? [];
      $data = serialize($selection_settings) . $element['#target_type'] //
              . $element['#role_id'] . $element['#project_id'] . $element['#selection_handler'];
      $selection_settings_key = Crypt::hmacBase64($data, Settings::getHashSalt());

      $key_value_storage = \Drupal::keyValue('entity_autocomplete');
      if (!$key_value_storage->has($selection_settings_key)) {
        $key_value_storage->set($selection_settings_key, $selection_settings);
      }

      $element['#autocomplete_route_parameters'] = [
          'base_entity_id' => $element['#role_id'],
          'next_entity_id' => $element['#project_id'],
          'target_type' => $element['#target_type'],
          'selection_handler' => $element['#selection_handler'],
          'selection_settings_key' => $selection_settings_key,
      ];

      if (isset($element['#process']) && !$element['#processed']) {
        foreach ($element['#process'] as $callback) {
          $callback = $form_state->prepareCallback($callback);
          $method = $callback[1] ?? FALSE;
          if ($method && ($method !== 'processEntityAutocomplete')) {
            $complete_form = &$form_state->getCompleteForm();
            $element = call_user_func_array($callback, [&$element, &$form_state, &$complete_form]);
          }
        }
        $element['#processed'] = TRUE;
      }
    }
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::buildContentResult();
   */
  protected function buildContentResult(&$form, FormStateInterface $form_state) {
    $slogxtData = &$form_state->get('slogxtData');
    $slogxtData['showDonBtn'] = TRUE;
    if ($this->isOnSelect()) {
      $slogxtData['runCommand'] = 'radios';
    } elseif ($this->isOnView()) {
      $this->prepareViewData();
      $this->prepareDialogTmpPreview($slogxtData, $this->header, $this->content);
      return parent::_doBuildDialogViewContentResult($form, $form_state);
    } else {
      $slogxtData['runCommand'] = 'wizardFormEdit';
      $op = $this->request->get('op', false);
      $op_save = ($op === (string) t('Save'));
      if ($op_save && !$form_state->hasAnyErrors()) {
        $slogxtData['wizardFinished'] = true;
        $slogxtData['onWizardFinished'] = $this->getOnWizardFinished();
      } else {
        $slogxtData['wizardFinalize'] = true;
      }
    }
    return parent::buildContentResult($form, $form_state);
  }

  /**
   */
  protected function prepareViewData() {
    $header = '..MyPreviewTitle';
    $description = FALSE;

    //
    list($node_type, $field_name) = SlogXt::getDescriptionNodeKeys();
    $node = Node::create([
                'type' => $node_type,
                'title' => '',
                'status' => 1,
    ]);
    if ($this->is_project_entity && $this->project) {
      $header = $this->project->label();
      $description = (string) $this->project->get('description')->value;
    } elseif ($this->has_entity && $this->entity) {
      $header = $this->entity->label();
      if ($this->entity->hasField('description')) {
        $description = (string) $this->entity->get('description')->value;
      }
    }

    //
    $this->header = $header;
    if ($description) {
      $node->set($field_name, $description);
    }
    $node_preview_controller = NodePreviewController::create(\Drupal::getContainer());
    $build = $node_preview_controller->view($node);
    $build['#prefix'] = '<div class="preview">';
    $build['#suffix'] = '</div>';
    $build['#pre_render'][] = [static::class, 'preRenderPreview'];
    $this->content = (string) \Drupal::service('renderer')->renderRoot($build);
  }

  /**
   */
  public static function trustedCallbacks() {
    return ['preRenderPreview'];
  }

  /**
   */
  protected function preRenderForm(&$form, FormStateInterface $form_state) {
    parent::preRenderForm($form, $form_state);

    if (!empty($form['actions']['submit'])) {
      $form['actions']['submit']['#value'] = t('Save');
      unset($form['actions']['delete']);
      unset($form['actions']['cancel']);
    }

    if ($this->isOnDelete()) {
      $msg = (string) t('You are about to delete %name', ['%name' => $this->entity->label()]);
      $msg .= '<hr />' . t('WARNING: this action cannot be undone.');
      $form['description']['#markup'] = SlogXt::htmlMessage($msg, 'warning');
    }
  }

  /**
   */
  protected function _affectedByBoard($board) {
    $affected_slogitems = [];
    $project_id = (integer) $board->get('pm_project')->target_id;
    if ($project_id && $project = PmProject::load($project_id)) {
      if ($slogitem = SlogXtPm::getLinkedSlogitemFromEntity($project)) {
        $sid = $slogitem->id();
        $affected_slogitems[$sid] = $slogitem;
      }
    }

    return $affected_slogitems;
  }

  /**
   */
  protected function _affectedByTask($task) {
    $affected_slogitems = [];

    // parent: story
    $task_id = $task->id();
    if ($stories = SlogXtPm::getStoriesByTaskId($task_id)) {
      foreach ($stories as $story) {
        if ($slogitem = SlogXtPm::getLinkedSlogitemFromEntity($story)) {
          $sid = $slogitem->id();
          $affected_slogitems[$sid] = $slogitem;
        }
      }
    }
    // boards
    if ($boards = SlogXtPm::getBoardsByComponentId('pm_task', $task_id)) {
      foreach ($boards as $board) {
        if ($slogitem = SlogXtPm::getLinkedSlogitemFromEntity($board)) {
          $sid = $slogitem->id();
          $affected_slogitems[$sid] = $slogitem;
        }
      }
    }
    return $affected_slogitems;
  }

  /**
   */
  protected function _affectedByComponent($component, $comp_entity) {
    $affected_slogitems = [];

    // parents
    $component_id = $comp_entity->id();
    if ($parents = SlogXtPm::getParentsByComponentId($component, $component_id)) {
      foreach ($parents as $parent) {
        if ($slogitem = SlogXtPm::getLinkedSlogitemFromEntity($parent)) {
          $sid = $slogitem->id();
          $affected_slogitems[$sid] = $slogitem;
        }
      }
    }
    // boards
    if ($boards = SlogXtPm::getBoardsByComponentId($component, $component_id)) {
      foreach ($boards as $board) {
        if ($slogitem = SlogXtPm::getLinkedSlogitemFromEntity($board)) {
          $sid = $slogitem->id();
          $affected_slogitems[$sid] = $slogitem;
        }
      }
    }
    return $affected_slogitems;
  }

  /**
   */
  protected function getAffectedAttachData($entity) {
    $affected_slogitems = [];
    $more = [];
    if ($slogitem = SlogXtPm::getLinkedSlogitemFromEntity($entity)) {
      $sid = $slogitem->id();
      $affected_slogitems[$sid] = $slogitem;
    }

    //
    $component = (string) $entity->getEntityTypeId();
    switch ($component) {
      case 'pm_board':
        $more = $this->_affectedByBoard($entity);
        break;
      case 'pm_task':
        $more = $this->_affectedByTask($entity);
        break;
      case 'pm_feature':
      case 'pm_story':
      case 'pm_sub_task':
        $more = $this->_affectedByComponent($component, $entity);
        break;
    }

    //
    if (!empty($more)) {
      $affected_slogitems += $more;
    }

    $affected_data = [];
    foreach ($affected_slogitems as $sid => $slogitem) {
      $affected_data[$sid] = $slogitem->getAttachData();
    }

    return $affected_data;
  }

  /**
   */
  protected function getOnWizardFinished() {
    $final_command = ['command' => 'slogxt::finishedIdleFunc'];
    $entity = $this->is_project_entity ? $this->project : $this->entity;
    if ($entity) {
      $affected_data = $this->getAffectedAttachData($entity);
      if (!empty($affected_data)) {
        $final_command = [
            'command' => 'sxt_slogitem::refreshAffectedContents',
            'args' => ['affectedSids' => $affected_data],
        ];
      }
    }

    $steps = 1;
    if ($this->isOnCreate()) {
      $types = SlogXtPm::getComponentBundles($this->component);
      if (count($types) > 1) {
        $steps = 2;
      }
    } elseif ($this->isOnDelete()) {
      $steps = 2;
    }
    //
    return $this->commandAddPrevPageCommand($final_command, $steps);
  }
}
