<?php

/**
 * @file
 * Contains \Drupal\sxt_pm\Form\SelectPmTypeForm
 */

namespace Drupal\sxt_pm\Form;

use Drupal\sxt_pm\SlogXtPm;
use Drupal\slogxt\Form\XtRadiosFormBase;

/**
 */
class SelectPmTypeForm extends XtRadiosFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'xtpm_select_type';
  }

  protected function isSingleLine() {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function getXtOptions() {
    $items = [];
    $request = \Drupal::request();
    $path_info = urldecode($request->getPathInfo());
    $pm_entity_id = $request->get('pmEntityTypeId');
    $replace_key = $request->get('pathReplaceKey');
    
    $types = SlogXtPm::getComponentBundles($pm_entity_id);
    foreach ($types as $typeid => $type) {
      $items[] = [
          'liTitle' => $type['label'],
          'entityid' => $typeid,
          'path' => slogxt_str_replace($replace_key, $typeid, $path_info),
      ];
    }

    return $items;
  }

}
