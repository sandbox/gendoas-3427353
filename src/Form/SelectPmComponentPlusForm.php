<?php

/**
 * @file
 * Contains \Drupal\sxt_pm\Form\SelectPmComponentPlusForm
 */

namespace Drupal\sxt_pm\Form;

use Drupal\sxt_pm\SlogXtPm;
use Drupal\slogxt\Form\XtRadiosFormBase;

/**
 */
class SelectPmComponentPlusForm extends XtRadiosFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'xtpm_select_entity_plus';
  }

  protected function isSingleLine() {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function getXtOptions() {
    $items = [];
    $request = \Drupal::request();
    $path_info = urldecode($request->getPathInfo());
    $for_type = (boolean) $request->get('forType', FALSE);
    $txt_type = $for_type ? t('Type') : '';
    if ($for_type) {
      $project_title = t('Project');
      $type_comps = SlogXtPm::getPmTypeLabels();
      foreach ($type_comps as $key => $label) {
        $component_id = $key;
        if ($key === 'pm_board_column') {
          $component_id = '_pm_board_column_';
        }
        $items[] = [
            'liTitle' => $label,
            'entityid' => 0,
            'path' => slogxt_str_replace('{component}', $component_id, $path_info),
        ];
      }
    } else {
      $project = $request->get('pmProject');
      if (empty($project)) {
        $items[] = [
            'liTitle' => 'Error: no project',
            'entityid' => 0,
            'path' => $path_info,
        ];
        return $items;
      }

      //
      $project_id = $project->id();
      $path_info = slogxt_str_replace('{project_id}', $project_id, $path_info);
      $project_title = $this->xtraOptTitle(t('Project'));
    }

    //
    $items[] = [
        'liTitle' => "$project_title $txt_type",
        'entityid' => 0,
        'path' => slogxt_str_replace('{component}', 'pm_project', $path_info),
    ];

    //
    $module_handler = \Drupal::moduleHandler();
    $pm_components = SlogXtPm::getComponentLabels($for_type);
    foreach ($pm_components as $key => $label) {
      $module = $key;
      if ($key === 'pm_board_column') {
        $module = 'pm_board';
      }
      if ($module_handler->moduleExists($module)) {
        $items[] = [
            'liTitle' => "$label $txt_type",
            'entityid' => 0,
            'path' => slogxt_str_replace('{component}', $key, $path_info),
        ];
      }
    }

    return $items;
  }
}
