<?php

/**
 * @file
 * Contains \Drupal\sxt_pm\Form\SelectPmProjectActionForm
 */

namespace Drupal\sxt_pm\Form;

use Drupal\sxt_pm\SlogXtPm;
use Drupal\slogxt\Form\XtRadiosFormBase;

/**
 */
class SelectPmProjectActionForm extends XtRadiosFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'xtpm_select_project_action';
  }

  protected function isSingleLine() {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function getXtOptions() {
    $items = [];
    $request = \Drupal::request();
    $path_info = urldecode($request->getPathInfo());
    $project = $request->get('pmProject');
    if (!$project) {
      throw new \LogicException('Missing project.');
    }

    // 
    $action_labels = SlogXtPm::getEditActionLabels();
    $project_id = $project->id();
    $items[] = [
        'liTitle' => t('View'),
        'entityid' => $project_id,
        'path' => slogxt_str_replace('{action}', 'pmview', $path_info),
    ];

    $items[] = [
        'liTitle' => t('Edit'),
        'entityid' => $project_id,
        'path' => slogxt_str_replace('{action}', 'edit', $path_info),
    ];

    $action_key = 'editstatus';
    $items[] = [
        'liTitle' => $action_labels[$action_key],
        'entityid' => $project_id,
        'path' => slogxt_str_replace('{action}', $action_key, $path_info),
    ];

    $action_key = 'editdate';
    $items[] = [
        'liTitle' => $action_labels[$action_key],
        'entityid' => $project_id,
        'path' => slogxt_str_replace('{action}', $action_key, $path_info),
    ];

    return $items;
  }
}
