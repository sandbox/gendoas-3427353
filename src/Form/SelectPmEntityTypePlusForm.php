<?php

/**
 * @file
 * Contains \Drupal\sxt_pm\Form\SelectPmEntityTypePlusForm
 */

namespace Drupal\sxt_pm\Form;

use Drupal\sxt_pm\SlogXtPm;
use Drupal\user\RoleInterface;
use Drupal\slogxt\Form\XtRadiosFormBase;

/**
 */
class SelectPmEntityTypePlusForm extends XtRadiosFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'xtpm_select_entity_plus';
  }

  protected function isSingleLine() {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function getXtOptions() {
    $items = [];
    $request = \Drupal::request();
    $path_info = urldecode($request->getPathInfo());
    $component = $request->get('component');

    // 
    if ($component !== 'pm_board_column') {
      $items[] = [
          'liTitle' => $this->xtraOptTitle(t('New')),
          'entityid' => 0,
          'path' => slogxt_str_replace('{action}', 'createnew', $path_info),
      ];
    }

    //
    if (in_array($component, ['pm_status', 'pm_priority'])) {
      $rid = RoleInterface::AUTHENTICATED_ID;
      $entities = SlogXtPm::getPmTypeEntities($component);
      foreach ($entities as $id => $entity) {
        $items[] = [
            'liTitle' => $entity->label(),
            'liDetails' => "['[XtPmProject:$component|id=$id|role=$rid]']",
            'entityid' => $id,
            'path' => slogxt_str_replace('{entity_id}', $id, $path_info),
        ];
      }     
    } else {
      $types = SlogXtPm::getComponentBundles($component);
      foreach ($types as $typeid => $type) {
        $items[] = [
            'liTitle' => $type['label'],
            'entityid' => $typeid,
            'path' => slogxt_str_replace('{entity_id}', $typeid, $path_info),
        ];
      }
    }

    return $items;
  }
}
