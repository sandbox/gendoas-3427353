<?php

/**
 * @file
 * Contains \Drupal\sxt_pm\Form\XtPmTypeEntityForm
 */

namespace Drupal\sxt_pm\Form;

use Drupal\sxt_slogitem\SlogXtsi;
use Drupal\slogxt\SlogXt;
use Drupal\user\Entity\Role;
use Drupal\Core\Render\Element;
use Drupal\user\RoleInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Security\TrustedCallbackInterface;
use Drupal\sxt_pm\Form\XtPmFormBase;

/**
 */
class XtPmTypeEntityForm extends XtPmFormBase implements TrustedCallbackInterface {

  /**
   * {@inheritdoc}
   */
  public function getBaseFormId() {
    $request = \Drupal::request();
    $this->role = Role::load(RoleInterface::AUTHENTICATED_ID);
    $this->entity = $request->get('pmTypeEntity', FALSE);
    $this->editAction = $request->get('editAction');
    $this->editOpt = $request->get('editOpt');
    $this->component = $request->get('component');
    $this->is_board_column = ($this->component === 'pm_board_column');
    $on_create_new = ($this->editOpt === 'add');
    if (!$this->entity) {
      throw new \LogicException('Missing entity.');
    }
    //
    $this->setModuleHandler(\Drupal::moduleHandler());
    return parent::getBaseFormId();
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);

    $to_remove = ['advanced', 'author', 'status', 'weight', 'created', 'footer', 'meta', 'uid'];
    switch ($this->editAction) {
      case 'createnew':
      case 'edit':
        $more = ['color'];
        $to_remove = array_merge($to_remove, $more);
        break;
      case 'editcolor':
        $more = ['label', 'description'];
        $to_remove = array_merge($to_remove, $more);
        break;
    }

    //
    foreach (Element::children($form) as $field_name) {
      if (in_array($field_name, $to_remove)) {
        unset($form[$field_name]);
      }
    }
    // no delete
    unset($form['actions']['delete']);

    return $form;
  }

  /**
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
    $slogxt = &$form_state->get('slogxt');
    $slogxt['previewHeaderKey'] = 'label';

    $entity = $this->entity;
    $this->entity_is_new = $entity->isNew();
    $values = & $form_state->getValues();
    $description = (string) $values['description'][0]['value'];
    if (!empty($description)) {
      $base_rootterm = SlogXtsi::getRoleDescriptionRootTerm($this->role);
      $cname = 'Main';
      $term_name = "PmProject.$cname";
      $menuterm = SlogXtsi::getDescriptionMenuTerm($base_rootterm, $term_name, TRUE);
      $this->storeTid = $menuterm->id();
      $for_entity = SlogXt::getDescriptionForEntity($entity);
      $description = SlogXtsi::fixDescriptionMoreLink($description, $this->storeTid, $for_entity);
      $values['description'][0]['value'] = $description;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $result = parent::save($form, $form_state);
    if ($result === SAVED_NEW && ($this->editAction === 'createnew')) {
      $this->fixDescriptionMoreLink();
    }

    return $result;
  }
}
