<?php

/**
 * @file
 * Contains \Drupal\sxt_pm\Form\SelectPmTypeActionForm
 */

namespace Drupal\sxt_pm\Form;

use Drupal\sxt_pm\SlogXtPm;
use Drupal\slogxt\Form\XtRadiosFormBase;

/**
 */
class SelectPmTypeActionForm extends XtRadiosFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'xtpm_select_type_action';
  }

  protected function isSingleLine() {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getXtOptions() {
    $items = [];
    $request = \Drupal::request();
    $path_info = urldecode($request->getPathInfo());
    $component = $request->get('component');
    $pm_type_entity = $request->get('pmTypeEntity', FALSE);
    $is_for_entity = (boolean) $request->get('isForEntity', FALSE);
    $is_status_or_priority = in_array($component, ['pm_status', 'pm_priority']);
    if (!$pm_type_entity) {
      throw new \LogicException('Missing entity.');
    }


    //
    $entity_id = $pm_type_entity->id();
    $entity_label = $pm_type_entity->label();
    if ($component === 'pm_project') {
      $component_label = t('Project');
    } elseif ($component === 'pm_board_column') {
      $component_label = t('Board Column');
    } else {
      $pmc_labels = $is_for_entity ? SlogXtPm::getPmTypeLabels() : SlogXtPm::getComponentLabels();
      $component_label = $pmc_labels[$component] ?? '???';
    }

    // 
    $args = [
        '%label' => $entity_label,
        '@comp' => $component_label,
        '@type' => !$is_for_entity ? ' ' . t('Type') : '',
    ];
    if ($is_status_or_priority) {
      $items[] = [
          'liTitle' => t('View'),
          'liDescription' => t('View @comp@type: %label', $args),
          'entityid' => $entity_id,
          'path' => slogxt_str_replace('{action}', 'view', $path_info),
      ];
    }

    $items[] = [
        'liTitle' => t('Edit'),
        'liDescription' => t('Edit @comp@type: %label', $args),
        'entityid' => $entity_id,
        'path' => slogxt_str_replace('{action}', 'edit', $path_info),
    ];

    if ($is_status_or_priority) {
      $items[] = [
          'liTitle' => t('Edit Color'),
          'liDescription' => t('Edit the color for @comp@type: %label', $args),
          'entityid' => $entity_id,
          'path' => slogxt_str_replace('{action}', 'editcolor', $path_info),
      ];
    }

    $items[] = [
        'liTitle' => t('Delete'),
        'liDescription' => t('Delete @comp@type: %label', $args),
        'entityid' => $entity_id,
        'path' => slogxt_str_replace('{action}', 'delete', $path_info),
    ];

    return $items;
  }
}
