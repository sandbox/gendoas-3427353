<?php

/**
 * @file
 * Contains \Drupal\sxt_pm\Form\SelectPmEntityActionForm
 */

namespace Drupal\sxt_pm\Form;

use Drupal\sxt_pm\SlogXtPm;
use Drupal\slogxt\Form\XtRadiosFormBase;

/**
 */
class SelectPmEntityActionForm extends XtRadiosFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'xtpm_select_entity_action';
  }

  protected function isSingleLine() {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getXtOptions() {
    $items = [];
    $request = \Drupal::request();
    $path_info = urldecode($request->getPathInfo());
    $component = $request->get('component');
    $is_board_column = ($component === 'pm_board_column');
    $pm_entity = $request->get('pmEntity', FALSE);
    if (!$pm_entity) {
      throw new \LogicException('Missing entity.');
    }


    //
    $entity_id = $pm_entity->id();
    $definitions = $pm_entity->getFieldDefinitions();
    $field_names = array_keys($definitions);
    $pm_components = SlogXtPm::getComponentLabels();
    $action_labels = SlogXtPm::getEditActionLabels();
    // 
    $args = [
        '%label' => $pm_entity->label(),
        '%comp' => $pm_components[$component] ?? '???',
    ];
    if ($component !== 'pm_board_column') {
      $items[] = [
          'liTitle' => t('View'),
          'liDescription' => t('View the component %comp (base data only)', $args),
          'entityid' => $entity_id,
          'path' => slogxt_str_replace('{action}', 'pmview', $path_info),
      ];
    }

    $items[] = [
        'liTitle' => t('Edit'),
        'liDescription' => t('Edit the base data (title and description)'),
        'entityid' => $entity_id,
        'path' => slogxt_str_replace('{action}', 'edit', $path_info),
    ];

    if (in_array('pm_status', $field_names)) {
      $action_key = 'editstatus';
      $items[] = [
          'liTitle' => $action_labels[$action_key],
          'liDescription' => t('Edit the fields status and priority.'),
          'entityid' => $entity_id,
          'path' => slogxt_str_replace('{action}', $action_key, $path_info),
      ];
    }

    if (in_array('pm_date', $field_names)) {
      $action_key = 'editdate';
      $items[] = [
          'liTitle' => $action_labels[$action_key],
          'liDescription' => t('Edit the date range fields.'),
          'entityid' => $entity_id,
          'path' => slogxt_str_replace('{action}', $action_key, $path_info),
      ];
    }

    if (!$is_board_column && in_array('pm_task', $field_names)) {
      $action_key = 'edittask';
      $items[] = [
          'liTitle' => $action_labels[$action_key],
          'liDescription' => t('Edit the task field.'),
          'entityid' => $entity_id,
          'path' => slogxt_str_replace('{action}', $action_key, $path_info),
      ];
    }

    if (in_array('pm_assignee', $field_names)) {
      $action_key = 'editassignee';
      $items[] = [
          'liTitle' => $action_labels[$action_key],
          'liDescription' => t('Edit the fields assignee and reviewer.'),
          'entityid' => $entity_id,
          'path' => slogxt_str_replace('{action}', $action_key, $path_info),
      ];
    }

    //list
    $list_labels = SlogXtPm::getComponentListLabels();
    foreach ($list_labels as $field_name => $field_label) {
      if (in_array($field_name, $field_names)) {
        $args['%list'] = $list_labels[$field_name] ?? '???';
        $desc = t('Edit the %list of %comp: %label', $args);
        $items[] = [
            'liTitle' => $field_label,
            'liDescription' => $desc,
            'entityid' => $entity_id,
            'path' => slogxt_str_replace('{action}', 'editlist', $path_info),
        ];
        break;
      }
    }

    $items[] = [
        'liTitle' => t('Delete'),
        'liDescription' => t('Delete the %comp component: %label', $args),
        'entityid' => $entity_id,
        'path' => slogxt_str_replace('{action}', 'pmdelete', $path_info),
    ];

    return $items;
  }
}
