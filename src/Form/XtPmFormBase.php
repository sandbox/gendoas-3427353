<?php

/**
 * @file
 * Contains \Drupal\sxt_pm\Form\XtPmFormBase
 */

namespace Drupal\sxt_pm\Form;

use Drupal\slogxt\SlogXt;
use Drupal\sxt_slogitem\SlogXtsi;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Security\TrustedCallbackInterface;
use Drupal\pm\PmContentEntityForm;
use Drupal\slogxt\XtExtrasTrait;
use Drupal\slogxt\Form\XtPreviewFormTrait;

/**
 */
abstract class XtPmFormBase extends PmContentEntityForm implements TrustedCallbackInterface {

  use XtExtrasTrait;
  use XtPreviewFormTrait;

  protected $project = NULL;
  protected $role = NULL;
  protected $editAction = FALSE;
  protected $onCreateProject = FALSE;
  protected $project_id = NULL;
  protected $projectTypeId = NULL;
  protected $component = NULL;
  protected $is_board_column = FALSE;
  protected $entity = NULL;
  protected $entity_is_new = FALSE;
  protected $storeTid = NULL;

  /**
   * {@inheritdoc}
   */
  public static function trustedCallbacks() {
    return ['preRender', 'preRenderPreview'];
  }

  /**
   */
  protected function initVars() {
    $request = \Drupal::request();
    $this->project = $request->get('pmProject', FALSE);
    $this->role = $request->get('projectRole', FALSE);
    $this->editAction = $request->get('editAction');
    $this->projectTypeId = $request->get('projectTypeId');
    $this->onCreateProject = (boolean) $request->get('onCreateProject', FALSE);
    if (!$this->onCreateProject) {
      if (!$this->project || !$this->role || !$this->editAction) {
        throw new \LogicException('Missing project or role or edit action.');
      }

      //
      $this->project_id = (integer) $this->project->id();
      $this->component = $request->get('component');
      $this->is_board_column = ($this->component === 'pm_board_column');
    }
  }

  /**
   */
  protected function actions(array $form, FormStateInterface $form_state) {
    $actions = parent::actions($form, $form_state);
    unset($actions['delete']);
    if (!$this->is_board_column && in_array($this->editAction, ['createnew', 'edit'])) {
      return $this->addPreviewAction($actions);
    }

    return $actions;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);
    $form['#pre_render'][] = [get_class($this), 'preRender'];
    return $form;
  }

  /**
   */
  public static function preRender($form) {
    if (!empty($form['description'])) {
      $d_widget = & $form['description']['widget'][0];
      unset($d_widget['format']);
      //
      $format = SlogXt::xtitemsFormatId();
      $d_widget['#editor'] = FALSE;  // no editor
      $d_widget['#format'] = $format;
      $d_widget['#allowed_formats'] = [$format];
      $d_widget['#maxlength'] = 240;
      $d_widget['value']['#description'] = t('Enter the description for the element (max. 240 characters).');
      $d_widget['value']['#attributes']['class'][] = 'sxt-maxheight-180';
    }
    //
    if (!empty($form['pm_project'])) {
      $p_widget = & $form['pm_project']['widget'][0];
      $p_widget['target_id']['#attributes']['class'][] = 'visually-hidden';
    }

    return $form;
  }

  /**
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
    $slogxt = &$form_state->get('slogxt');
    $slogxt['previewHeaderKey'] = 'label';

    $entity = $this->entity;
    $this->entity_is_new = $entity->isNew();
    $values = & $form_state->getValues();
    $description = (string) $values['description'][0]['value'];
    if (!empty($description)) {
      $base_rootterm = SlogXtsi::getRoleDescriptionRootTerm($this->role);
      $list_labels = [
          'pm_feature' => t('Feature'),
          'pm_story' => t('Story'),
          'pm_task' => t('Task'),
          'pm_sub_task' => t('SubTask'),
      ];
      $cname = 'Main';
      if ($this->component && in_array($this->component, array_keys($list_labels))) {
        $cname = $list_labels[$this->component];
      }
      $term_name = "PmProject.$cname";
      $menuterm = SlogXtsi::getDescriptionMenuTerm($base_rootterm, $term_name, TRUE);
      $this->storeTid = $menuterm->id();
      $for_entity = SlogXt::getDescriptionForEntity($entity);
      $description = SlogXtsi::fixDescriptionMoreLink($description, $this->storeTid, $for_entity);
      $values['description'][0]['value'] = $description;
    }
  }

  /**
   */
  protected function fixDescriptionMoreLink() {
    if ($this->entity->hasField('description')) {
      $for_entity = SlogXt::getDescriptionForEntity($this->entity);
      $description = $this->entity->description->value;
      $description = SlogXtsi::fixDescriptionMoreLink($description, $this->storeTid, $for_entity);
      $this->entity->description->value = $description;
      $this->entity->save();
    }
  }
}
