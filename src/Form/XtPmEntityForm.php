<?php

/**
 * @file
 * Contains \Drupal\sxt_pm\Form\XtPmEntityForm.
 */

namespace Drupal\sxt_pm\Form;

use Drupal\sxt_pm\SlogXtPm;
use Drupal\Core\Render\Element;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Security\TrustedCallbackInterface;
use Drupal\sxt_pm\Form\XtPmFormBase;

/**
 */
class XtPmEntityForm extends XtPmFormBase implements TrustedCallbackInterface {

  /**
   * {@inheritdoc}
   */
  public function getBaseFormId() {
    $this->initVars();

    //
    $request = \Drupal::request();
    $pm_entity = $request->get('pmEntity', FALSE);
    $component = $request->get('component');
    $this->entityTypeId = $request->get('entityTypeId');
    $on_create_new = ($this->editAction === 'createnew');

    if ($pm_entity && $this->editAction) {
      $this->entity = $pm_entity;
    } elseif ($on_create_new && $this->entityTypeId) {
      $values = [
          'bundle' => $this->entityTypeId,
          'project_id' => $this->project_id,
      ];
      $this->entity = \Drupal::entityTypeManager()
              ->getStorage($component)
              ->create($values);
    } else {
      throw new \LogicException('Missing init data.');
    }

    //
    $this->setModuleHandler(\Drupal::moduleHandler());
    return parent::getBaseFormId();
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);
    // pm_project
    $form['pm_project']['#attributes']['class'][] = 'visually-hidden';
    $target_id = & $form['pm_project']['widget'][0]['target_id'];
    $target_id['#value'] = $this->project_id;
    $target_id['#type'] = 'textfield';
    unset($target_id['#target_type']);

    $to_remove = ['advanced', 'author', 'created', 'footer', 'meta', 'pm_references', 'pm_member_list', 'uid'];
    if ($this->editAction !== 'editlist') {
      $list_labels = SlogXtPm::getComponentListLabels();
      $to_remove_lists = array_keys($list_labels);
    }
    switch ($this->editAction) {
      case 'createnew':
      case 'edit':
        $more = ['pm_date', 'pm_priority', 'pm_status', 'pm_assignee', 'pm_reviewer'];
        if (!$this->is_board_column) {
           $more[] = 'pm_task'; 
        }
        $to_remove = array_merge($to_remove, $more, $to_remove_lists);
        break;
      case 'editstatus':
        $more = ['label', 'description', 'pm_date', 'pm_assignee', 'pm_reviewer', 'pm_task'];
        $to_remove = array_merge($to_remove, $more, $to_remove_lists);
        break;
      case 'editdate':
        $more = ['label', 'description', 'pm_priority', 'pm_status', 'pm_assignee', 'pm_reviewer', 'pm_task'];
        $to_remove = array_merge($to_remove, $more, $to_remove_lists);
        break;
      case 'edittask':
        $more = ['label', 'description', 'pm_date', 'pm_priority', 'pm_status', 'pm_assignee', 'pm_reviewer'];
        $to_remove = array_merge($to_remove, $more, $to_remove_lists);
        break;
      case 'editassignee':
        $more = ['label', 'description', 'pm_date', 'pm_priority', 'pm_status', 'pm_task'];
        $to_remove = array_merge($to_remove, $more, $to_remove_lists);
        break;
      case 'editlist':
        $more = ['label', 'description', 'pm_date', 'pm_priority', 'pm_status', 'pm_assignee', 'pm_reviewer', 'pm_task'];
        $to_remove = array_merge($to_remove, $more);
        break;
    }

    foreach (Element::children($form) as $field_name) {
      if (in_array($field_name, $to_remove)) {
        unset($form[$field_name]);
      }
    }

    // no delete
    unset($form['actions']['delete']);

    //
    $field_wrapper = $this->getInputFieldWrapper(FALSE, $extra);
    $to_wrap = ['label', 'description'];
    foreach (Element::children($form) as $field_name) {
      if (in_array($field_name, $to_wrap)) {
        $form[$field_name] += $field_wrapper;
      }
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $result = parent::save($form, $form_state);
    if ($result === SAVED_NEW && ($this->editAction === 'createnew')) {
      $this->fixDescriptionMoreLink();
    }

    return $result;
  }
}
