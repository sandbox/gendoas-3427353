<?php

/**
 * @file
 * Contains \Drupal\sxt_pm\Form\SelectPmEntityPlusForm
 */

namespace Drupal\sxt_pm\Form;

use Drupal\sxt_pm\SlogXtPm;
use Drupal\sxt_slogitem\SlogXtsi;
use Drupal\slogxt\XtUserData;
use Drupal\user\RoleInterface;
use Drupal\slogxt\Form\XtRadiosFormBase;

/**
 */
class SelectPmEntityPlusForm extends XtRadiosFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'xtpm_select_entity_plus';
  }

  protected function isSingleLine() {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getXtOptions() {
    $items = [];
    $request = \Drupal::request();
    $path_info = urldecode($request->getPathInfo());
    $component = $request->get('component');
    $project_id = $request->get('project_id', FALSE);
    $for_config_entity = (boolean) $request->get('forConfigEntity');
    $is_role_pmadmin = (boolean) $request->get('isRolePmAdmin');
    //
    $pmc_labels = $for_config_entity ? SlogXtPm::getPmTypeLabels() : SlogXtPm::getComponentLabels();
    $component_label = $pmc_labels[$component] ?? $component;

    // 
    if ($is_role_pmadmin) {
      $items[] = [
          'liTitle' => $this->xtraOptTitle(t('New')),
          'liDescription' => t('Create new component: %type', ['%type' => $component_label]),
          'entityid' => 0,
          'path' => slogxt_str_replace('{action}', 'createnew', $path_info),
      ];
    }

    //
    if ($for_config_entity) {
      $rid = RoleInterface::AUTHENTICATED_ID;
      $entities = SlogXtPm::getPmTypeEntities($component);      
    } else {
      $role = XtUserData::getDefaultRole(\Drupal::currentUser(), TRUE);
      $rid = $role->id();
      $entities = SlogXtPm::getComponentEntities($component, $project_id);
    }

    foreach ($entities as $id => $entity) {
      $description = $entity->hasField('description') ? $entity->description->value : 'XXXXX';
      $description =  SlogXtsi::removeMoreLinkData($description, '(@) ');
      $items[] = [
          'liTitle' => $entity->label(),
          'liDescription' => $description,
          'liDetails' => "['[XtPmProject:$component|id=$id|role=$rid]']",
          'entityid' => $id,
          'path' => slogxt_str_replace('{entity_id}', $id, $path_info),
      ];
    }

    return $items;
  }
}
