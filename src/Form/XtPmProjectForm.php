<?php

/**
 * @file
 * Contains \Drupal\sxt_pm\Form\XtPmProjectForm.
 */

namespace Drupal\sxt_pm\Form;

use Drupal\pm_project\Entity\PmProject;
use Drupal\Core\Render\Element;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Security\TrustedCallbackInterface;
use Drupal\sxt_pm\Form\XtPmFormBase;

/**
 */
class XtPmProjectForm extends XtPmFormBase implements TrustedCallbackInterface {

  /**
   * {@inheritdoc}
   */
  public function getBaseFormId() {
    $this->initVars();
    
    if (!$this->onCreateProject && $this->editAction) {
      $this->entity = $this->project;
    } elseif ($this->projectTypeId) {
      $this->entity = PmProject::create(['bundle' => $this->projectTypeId]);
    } else {
      throw new \LogicException('Missing init data.');
    }

    //
    $this->setModuleHandler(\Drupal::moduleHandler());
    return parent::getBaseFormId();
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);

    $to_remove = ['advanced', 'author', 'created', 'footer', 'meta', 'pm_references', 'pm_member_list', 'uid'];
    switch ($this->editAction) {
      case 'createnew':
      case 'edit':
        $to_remove = array_merge($to_remove, ['pm_date', 'pm_priority', 'pm_status']);
        break;
      case 'editstatus':
        $to_remove = array_merge($to_remove, ['label', 'description', 'pm_date']);
        break;
      case 'editdate':
        $to_remove = array_merge($to_remove, ['label', 'description', 'pm_priority', 'pm_status']);
        break;
    }

    foreach (Element::children($form) as $field_name) {
      if (in_array($field_name, $to_remove)) {
        unset($form[$field_name]);
      }
    }

    // no delete
    unset($form['actions']['delete']);

    //
    $field_wrapper = $this->getInputFieldWrapper(FALSE, $extra);
    $to_wrap = ['label', 'description'];
    foreach (Element::children($form) as $field_name) {
      if (in_array($field_name, $to_wrap)) {
        $form[$field_name] += $field_wrapper;
      }
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $result = parent::save($form, $form_state);
    if ($result === SAVED_NEW && ($this->editAction === 'createnew')) {
      $project_id = (integer) $this->entity->id();
      $this->role->set('sxt_pm_project', $project_id)->save();
      //
      $this->fixDescriptionMoreLink();
    }

    return $result;
  }
}
