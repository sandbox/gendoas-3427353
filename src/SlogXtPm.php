<?php

/**
 * @file
 * Contains \Drupal\sxt_pm\SlogXtPm
 */

namespace Drupal\sxt_pm;

use Drupal\pm_project\Entity\PmProject;
use Drupal\sxt_slogitem\SlogXtsi;
use Drupal\sxt_slogitem\XtsiNodeTypeData;
use Drupal\Core\Entity\EntityInterface;

/**
 */
class SlogXtPm {

  protected static $entities = [];

  /**
   */
  public static function getEditActionLabels() {
    return [
        'editstatus' => t('Edit Status'),
        'editdate' => t('Edit Date'),
        'edittask' => t('Edit Task'),
        'editassignee' => t('Edit Assignee'),
        'editlist' => t('Edit List'),
    ];
  }

  /**
   */
  public static function getComponentListLabels() {
    return [
        'pm_board_column' => t('Board Column List'),
        'pm_feature_list' => t('Feature List'),
        'pm_story_list' => t('Story List'),
        'pm_sub_task_list' => t('SubTask List'),
    ];
  }

  /**
   */
  public static function getPmTypeLabels() {
    return [
        'pm_status' => t('Status'),
        'pm_priority' => t('Priority'),
        'pm_board_column' => t('Board Column'),
    ];
  }

  /**
   */
  public static function getComponentLabels($for_type = FALSE) {
    $labels = [
        'pm_board' => t('Board'),
        'pm_epic' => t('Epic'),
        'pm_feature' => t('Feature'),
        'pm_story' => t('Story'),
        'pm_task' => t('Task'),
        'pm_sub_task' => t('Sub Task'),
//        'pm_timetracking' => t('Time-tracking'),
//        'pm_expense' => t('Expense'),
//        'pm_invoice' => t('Invoice'),
    ];

    if ($for_type) {
      $labels = array_merge(['pm_board_column' => t('Board Column')], $labels);
    }
    return $labels;
  }

  /**
   */
  public static function getTargetListKey($component) {
    $list_keys = self::getTargetListKeys();
    return $list_keys[$component];
  }

  /**
   */
  public static function getTargetListKeys() {
    return [
        'pm_board' => 'pm_board_column',
        'pm_epic' => 'pm_feature_list',
        'pm_feature' => 'pm_story_list',
        'pm_task' => 'pm_sub_task_list',
    ];
  }

  /**
   */
  public static function getComponentBundles($component) {
    $info = \Drupal::service('entity_type.bundle.info');
    return $info->getBundleInfo($component);
  }

  /**
   */
  public static function getBoardColumnEntity($entity_id) {
    $entities = self::getPmTypeEntities('pm_board_column');
    return $entities[$entity_id];
  }

  /**
   */
  public static function getStatusEntity($entity_id) {
    $entities = self::getComponentEntities('pm_status');
    return $entities[$entity_id];
  }

  /**
   */
  public static function getPriorityEntity($entity_id) {
    $entities = self::getComponentEntities('pm_priority');
    return $entities[$entity_id];
  }

  /**
   */
  public static function canDeleteTypeEntity($component, $entity) {
    $entity_id = $entity->id();
//    $default = str_replace('xxpm_', '', "xx{$component}");
//    if ($entity_id === $default) {
//      return FALSE;
//    }

    if (in_array($component, ['pm_status', 'pm_priority'])) {
      $tables = [
          'pm_project',
          'pm_epic',
          'pm_feature',
          'pm_story',
          'pm_task',
          'pm_sub_task',
      ];
      $can_delete = TRUE;
      foreach ($tables as $table) {
        $ids = \Drupal::entityTypeManager()
                ->getStorage($table)
                ->getQuery()
                ->accessCheck(FALSE)
                ->condition($component, $entity_id)
                ->range(0, 1)
                ->execute();
        if (!empty($ids)) {
          $can_delete = FALSE;
          break;
        }
      }
    } else {
      $ids = \Drupal::entityTypeManager()
              ->getStorage($component)
              ->getQuery()
              ->accessCheck(FALSE)
              ->condition('bundle', $entity_id)
              ->range(0, 1)
              ->execute();
      $can_delete = empty($ids);
    }

    return $can_delete;
  }

  /**
   */
  public static function getPmTypeEntities($pm_component, $limit = 30) {
    $key = "$pm_component::$pm_component";
    if (!isset(self::$entities[$key])) {
      $storage = \Drupal::entityTypeManager()->getStorage($pm_component);
      $entity_ids = $storage->getQuery()
              ->accessCheck(FALSE)
              ->range(0, $limit)
              ->sort('changed', 'DESC')
              ->execute();
      self::$entities[$key] = $storage->loadMultiple($entity_ids);
    }
    return self::$entities[$key];
  }

  /**
   */
  public static function getComponentEntities($pm_component, $pm_project = NULL, $limit = 30) {
    $project_key = $pm_project ?? 'none';
    $project_id = (integer) $pm_project;
    $key = "$pm_component::$project_key";
    if (!isset(self::$entities[$key])) {
      $storage = \Drupal::entityTypeManager()->getStorage($pm_component);
      $query = $storage->getQuery()
              ->accessCheck(FALSE)
              ->range(0, $limit)
              ->sort('changed', 'DESC');
      $with_project = !in_array($pm_component, ['pm_status', 'pm_priority', 'pm_board_column']);
      if ($with_project && $project_id) {
        $query->condition('pm_project', $project_id);
      }
      $entity_ids = $query->execute();
      self::$entities[$key] = $storage->loadMultiple($entity_ids);
    }
    return self::$entities[$key];
  }

  /**
   */
  public static function getStoriesByTaskId($task_id, $limit = 30) {
    $storage = \Drupal::entityTypeManager()->getStorage('pm_story');
    $story_ids = $storage->getQuery()
            ->accessCheck(FALSE)
            ->condition('pm_task', $task_id)
            ->range(0, $limit)
            ->execute();
    if (!empty($story_ids)) {
      return $storage->loadMultiple($story_ids);
    }
    return FALSE;
  }

  /**
   */
  protected static function getParentComponent($component) {
    if (in_array($component, ['pm_sub_task', 'pm_task', 'pm_story', 'pm_feature'])) {
      if ($component === 'pm_sub_task') {
        $parent_table = "pm_task";
      } elseif ($component === 'pm_task') {
        $parent_table = "pm_story";
      } elseif ($component === 'pm_story') {
        $parent_table = "pm_feature";
      } else { //pm_feature
        $parent_table = "pm_epic";
      }
      return $parent_table;
    }
    return FALSE;
  }

  /**
   */
  protected static function getParentsByComponentId($component, $component_id) {
    if (!in_array($component, ['pm_sub_task', 'pm_story', 'pm_feature'])) {
      throw new \LogicException('Not a valid component.');
    }

    //
    $parent_table = self::getParentComponent($component);
    $list_key = "{$component}_list";
    $target_key = "{$list_key}_target_id";
    $table = "{$parent_table}__{$list_key}";
    $entity_ids = \Drupal::database()->select($table, 'tbl')
            ->fields('tbl', ['entity_id'])
            ->condition("tbl.$target_key", $component_id)
            ->execute()
            ->fetchCol();

    if (!empty($entity_ids)) {
      return \Drupal::entityTypeManager()
                      ->getStorage($parent_table)
                      ->loadMultiple($entity_ids);
    }

    return FALSE;
  }

  /**
   */
  protected static function getBoardsByComponentId($component, $component_id) {
    $component = self::getParentComponent($component);
    if (!in_array($component, ['pm_task', 'pm_story', 'pm_feature', 'pm_epic'])) {
      throw new \LogicException('Not a valid component.');
    }

    $board = 'pm_board';
    $board_column = 'pm_board_column';
    $board_column_table = "{$board_column}__{$component}";
    $target_key = "{$component}_target_id";
    $column_ids = \Drupal::database()->select($board_column_table, 'tbl')
            ->fields('tbl', ['entity_id'])
            ->condition("tbl.$target_key", $component_id)
            ->execute()
            ->fetchCol();

    if (!empty($column_ids)) {
      $board_column_table = "{$board}__{$board_column}";
      $target_key = "{$board_column}_target_id";
      $entity_ids = \Drupal::database()->select($board_column_table, 'tbl')
              ->fields('tbl', ['entity_id'])
              ->condition("tbl.$target_key", $column_ids, 'IN')
              ->execute()
              ->fetchCol();

      if (!empty($entity_ids)) {
        return \Drupal::entityTypeManager()
                        ->getStorage($board)
                        ->loadMultiple($entity_ids);
      }
    }

    return FALSE;
  }

  /**
   */
  public static function getListEntities($pm_component, $base_entity) {
    $bcomponent = $base_entity->getEntityTypeId();
    $bentity_id = $base_entity->id();
    $key = "$bcomponent::list::$bentity_id";

    if (!isset(self::$entities[$key])) {
      self::$entities[$key] = [];
      $list_ids = self::getListIdsByEntity($base_entity);
      if (!empty($list_ids)) {
        self::$entities[$key] = \Drupal::entityTypeManager()
                ->getStorage($pm_component)
                ->loadMultiple($list_ids);
      }
    }

    return self::$entities[$key];
  }

  /**
   */
  public static function getListIdsByEntity($entity) {
    $list_ids = [];
    $component = $entity->getEntityTypeId();
    $list_key = self::getTargetListKey($component);
    if ($entity->hasField($list_key)) {
      $list = $entity->get($list_key)->getValue();
      foreach ($list as $item) {
        $list_ids[] = $item['target_id'];
      }
    }
    return $list_ids;
  }

  /**
   */
  public static function ensureTypeId($entity_type_id) {
    if (!in_array($entity_type_id, ['pm_status', 'pm_priority'])) {
      $test = substr($entity_type_id, -5);
      if ($test !== '_type') {
        $entity_type_id = "{$entity_type_id}_type";
      }
    }
    return $entity_type_id;
  }

  /**
   */
  public static function loadTypeEntity($entity_type_id, $entity_id) {
    $entity_type_id = self::ensureTypeId($entity_type_id);
    return \Drupal::entityTypeManager()
                    ->getStorage($entity_type_id)
                    ->load($entity_id);
  }

  /**
   */
  public static function createTypeEntity($entity_type_id) {
    $entity_type_id = self::ensureTypeId($entity_type_id);
    return \Drupal::entityTypeManager()
                    ->getStorage($entity_type_id)
                    ->create();
  }

  /**
   */
  public static function getLinkedSlogitemFromEntity(EntityInterface $entity) {
    $slogitem = FALSE;
    if ($entity->hasField('description')) {
      $description = $entity->description->value;
      $parsed = SlogXtsi::parseMoreLinkData($description, 'XtsiMoreLink', ['sid']);
      $parsed = $parsed ? reset($parsed) : FALSE;
      if ($parsed && $parsed['type_valid']) {
        $slogitem = SlogXtsi::loadSlogitem($parsed['id']);
      }
    }

    return $slogitem;
  }

  /**
   */
  protected static function setXtPmValue(EntityInterface $slogitem, $value) {
    $node = $slogitem->getTargetEntity();
    if ($node && XtsiNodeTypeData::isXtNode($node)) {
      $xtra = XtsiNodeTypeData::nodeGetXtra($node);
      $xtra['xtpm'] = $value;
      XtsiNodeTypeData::nodeSetXtra($node, $xtra)->save();
    }
  }

  /**
   */
  protected static function _onBoardUpdate(EntityInterface $board) {
    $project_id = (integer) $board->get('pm_project')->target_id;
    if ($project_id && $project = PmProject::load($project_id)) {
      if ($slogitem = self::getLinkedSlogitemFromEntity($project)) {
        self::setXtPmValue($slogitem, $board->getChangedTime());
      }
    }
  }

  /**
   */
  protected static function _onTaskUpdate(EntityInterface $task) {
    // parent: story
    $task_id = $task->id();
    $xtpm_value = $task->getChangedTime();
    if ($stories = self::getStoriesByTaskId($task_id)) {
      foreach ($stories as $story) {
        if ($slogitem = self::getLinkedSlogitemFromEntity($story)) {
          self::setXtPmValue($slogitem, $xtpm_value);
        }
      }
    }
    // boards
    if ($boards = self::getBoardsByComponentId('pm_task', $task_id)) {
      foreach ($boards as $board) {
        if ($slogitem = self::getLinkedSlogitemFromEntity($board)) {
          self::setXtPmValue($slogitem, $xtpm_value);
        }
      }
    }
  }

  /**
   */
  protected static function _onComponentUpdate($component, EntityInterface $comp_entity) {
    // parents
    $component_id = $comp_entity->id();
    $xtpm_value = $comp_entity->getChangedTime();
    if ($parents = self::getParentsByComponentId($component, $component_id)) {
      foreach ($parents as $parent) {
        if ($slogitem = self::getLinkedSlogitemFromEntity($parent)) {
          self::setXtPmValue($slogitem, $xtpm_value);
        }
      }
    }
    // boards
    if ($boards = self::getBoardsByComponentId($component, $component_id)) {
      foreach ($boards as $board) {
        if ($slogitem = self::getLinkedSlogitemFromEntity($board)) {
          self::setXtPmValue($slogitem, $xtpm_value);
        }
      }
    }
  }

  /**
   */
  public static function onEntityUpdate(EntityInterface $entity) {
    if ($slogitem = self::getLinkedSlogitemFromEntity($entity)) {
      self::setXtPmValue($slogitem, $entity->getChangedTime());
    }

    //
    $component = (string) $entity->getEntityTypeId();
    switch ($component) {
      case 'pm_board':
        self::_onBoardUpdate($entity);
        break;
      case 'pm_task':
        self::_onTaskUpdate($entity);
        break;
      case 'pm_feature':
      case 'pm_story':
      case 'pm_sub_task':
        self::_onComponentUpdate($component, $entity);
        break;
    }
  }

  /**
   */
  public static function logger() {
    return \Drupal::logger('sxt_pm');
  }
}
