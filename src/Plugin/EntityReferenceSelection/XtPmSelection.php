<?php

/**
 * @file
 * Contains \Drupal\sxt_pm\Plugin\EntityReferenceSelection\XtPmSelection
 */

namespace Drupal\sxt_pm\Plugin\EntityReferenceSelection;

use Drupal\Core\Entity\Plugin\EntityReferenceSelection\DefaultSelection;

/**
 * @EntityReferenceSelection(
 *   id = "default:sxt_pm",
 *   label = @Translation("XtPm Selection"),
 *   entity_types = {
 *     "pm_feature", 
 *     "pm_story", 
 *     "pm_task", 
 *     "pm_sub_task", 
 *     "pm_timetracking",
 *     "pm_expense",
 *     "pm_invoice"
 *   },
 *   group = "default",
 *   weight = 10
 * )
 */
class XtPmSelection extends DefaultSelection {

  /**
   * {@inheritdoc}
   */
  protected function buildEntityQuery($match = NULL, $match_operator = 'CONTAINS') {
    $query = parent::buildEntityQuery($match, $match_operator);
    //
    $request = \Drupal::request();
    $target_type = $request->get('target_type');
    $pm_types = [
        'pm_feature',
        'pm_story',
        'pm_task',
        'pm_sub_task',
    ];
    $project_id = (integer) $request->get('next_entity_id');
    if (!$project_id) {
      $project_id = (integer) $request->get('pm-project');
    } 
    
    //
    if ($project_id && in_array($target_type, $pm_types)) {
        $query->condition('pm_project', $project_id);
    }

    return $query;
  }
}
