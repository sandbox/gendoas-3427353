<?php

/**
 * @file
 * Contains \Drupal\sxt_pm\Plugin\EntityReferenceSelection\XtPmUserSelection
 */

namespace Drupal\sxt_pm\Plugin\EntityReferenceSelection;

use Drupal\user\Entity\Role;
use Drupal\user\Plugin\EntityReferenceSelection\UserSelection;

/**
 * @EntityReferenceSelection(
 *   id = "default:sxt_pm_user",
 *   label = @Translation("XtPm User Selection"),
 *   entity_types = {"user"},
 *   group = "default",
 *   weight = 10
 * )
 */
class XtPmUserSelection extends UserSelection {

  /**
   * {@inheritdoc}
   */
  protected function buildEntityQuery($match = NULL, $match_operator = 'CONTAINS') {
    $request = \Drupal::request();
    $target_type = $request->get('target_type');
    $role_id = $request->get('base_entity_id') ?? FALSE;
    $role = $role_id ?  Role::load($role_id) : FALSE;
    $project_id = (integer) $request->get('next_entity_id');
    if (!$project_id) {
      $project_id = (integer) $request->get('pm-project');
    }

    //
    if ($target_type === 'user' && $role && $project_id) {
      $configuration = $this->getConfiguration();
      $configuration['filter']['role'] = $role_id;
      $configuration['filter']['type'] = $role->getEntityTypeId();
      $this->setConfiguration($configuration);
    }

    return parent::buildEntityQuery($match, $match_operator);
  }
}
