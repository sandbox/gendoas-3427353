<?php

/**
 * @file
 * Contains \Drupal\sxt_pm\Plugin\Filter\XtPmProject.
 */

namespace Drupal\sxt_pm\Plugin\Filter;

use Drupal\sxt_pm\SlogXtPm;
use Drupal\sxt_slogitem\SlogXtsi;
use Drupal\slogxt\SlogXt;
use Drupal\pm_project\Entity\PmProject;
use Drupal\user\Entity\Role;
use Drupal\node\Entity\Node;
use Drupal\node\Controller\NodePreviewController;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\sxt_slogitem\Plugin\Filter\XtsiMoreLinkFilter;
use Drupal\Core\Security\TrustedCallbackInterface;

/**
 * @Filter(
 *   id = "sxt_pm_project",
 *   title = @Translation("XtPmProject"),
 *   description = @Translation("Build ..."),
 *   type = Drupal\filter\Plugin\FilterInterface::TYPE_MARKUP_LANGUAGE,
 * )
 */
class XtPmProject extends XtsiMoreLinkFilter implements TrustedCallbackInterface {

  protected $role = NULL;
  protected $project = NULL;
  protected $project_id = NULL;
  protected $comp_labels = NULL;
  protected $list_keys = NULL;
  protected $list_labels = NULL;

  /**
   */
  protected function getKey() {
    return 'XtPmProject';
  }

  /**
   * Overrides XtsiMoreLinkFilter::getValidTypes().
   */
  protected function getValidTypes() {
    $valid_types = ['pm_project', 'pm_status', 'pm_priority'];
    $this->comp_labels = SlogXtPm::getComponentLabels();
    return array_merge($valid_types, array_keys($this->comp_labels));
  }

  /**
   */
  protected function getDoBypass($data) {
    static $do_bypass = [];
    $id = (integer) $data['id'];
    if (empty($do_bypass[$id]) && !empty($data['bypass'])) {
      $do_bypass[$id] = TRUE;
    }
    return !empty($do_bypass[$id]);
  }

  /**
   */
  protected function prepareMatchData($data, $import_is_pending) {
//todo::now - filter/XtPmProject
    $component = $data['type'];
    $entity_id = (integer) $data['id'];
    $is_project_type = ($component === 'pm_project');
    //
    $this->role = $role = Role::load($data['role']);
    $rolepid = (integer) $role->get('sxt_pm_project');
    $project_id = $is_project_type ? $entity_id : $rolepid;
    $project = PmProject::load($project_id);
    if (!$role || !$project) {
      $err_msg = (string) t('Missing role or project.');
      SlogXtsi::logger()->warning($err_msg);
      return FALSE;
    }

    if (!$err_msg && $is_project_type && ($rolepid !== $entity_id)) {
      $args = [
          '@rolepid' => $rolepid,
          '@datapid' => $entity_id,
      ];
      $err_msg = (string) t('ProjectID mismatch (role.pid=@rolepid, data.pid=@datapid)', $args);
    }

    $this->project_id = $project->id();
    $this->list_keys = SlogXtPm::getTargetListKeys();
    $this->list_labels = SlogXtPm::getComponentListLabels();
    $entity = $project;
    if (!$err_msg && !$is_project_type) {
      if (in_array($component, ['pm_status', 'pm_priority'])) {
        $more_labels = SlogXtPm::getPmTypeLabels();
        $this->comp_labels = array_merge($this->comp_labels, $more_labels);
        $entities = SlogXtPm::getPmTypeEntities($component);
      } else {
        $entities = SlogXtPm::getComponentEntities($component, $project_id);
      }
      $entity = $entities[$entity_id];
      if (!$entity) {
        $args = [
            '@rolepid' => $rolepid,
            '@datapid' => $entity_id,
        ];
        $err_msg = (string) t('Missing entity (role.pid=@rolepid, data.pid=@datapid)', $args);
      }
    }

    if ($err_msg) {
      $build = [
          '#prefix' => '<div class="sxtpm-project">',
          '#suffix' => '</div>',
          '#markup' => SlogXt::htmlMessage($err_msg, 'error'),
      ];
    } else {
      $cls = '"' . "sxtpm-project $component" . '"';
      $clabel = $is_project_type ? t('Project') : $this->comp_labels[$component] ?? '???';
      $elabel = $entity->label();
      $build = [
          '#prefix' => "<div class=$cls>",
          '#suffix' => '</div>',
          'section00' => $this->_buildSection("$clabel: $elabel", $entity->id()),
      ];

      $lkey = $this->list_keys[$component];
      switch ($component) {
        case 'pm_project':
          $build['description'] = $this->_buildDescription($project);
          $build['status'] = $this->_buildStatusPriority($project);
          $build['daterange'] = $this->_buildDateRange($project);
          foreach (['pm_board'] as $pmc_id) {
            if ($this->hasEntities($pmc_id)) {
              $slabel = (string) $this->comp_labels[$pmc_id];
              $build["s_{$pmc_id}"] = $this->_buildSection($slabel);
              $build[$pmc_id] = $this->_buildComponents($pmc_id);
            }
          }
          break;
        case 'pm_board':
          $build['description'] = $this->_buildDescription($entity);
          $build['daterange'] = $this->_buildDateRange($entity);
          if ($list_build = $this->_buildBoardColumnList($entity)) {
            foreach ($list_build as $key => $value) {
              $build["$key.title"] = $this->_buildSection($value['title']);
              $build["$key.content"] = $value['content'];
            }
          }
          break;
        case 'pm_epic':
        case 'pm_feature':
        case 'pm_story':
        case 'pm_task':
        case 'pm_sub_task':
          $build['description'] = $this->_buildDescription($entity);
          $build['status'] = $this->_buildStatusPriority($entity);
          $build['daterange'] = $this->_buildDateRange($entity);
          $list_build = FALSE;
          if ($component === 'pm_epic') {
            $list_build = $this->_buildFeatureList($entity);
          } elseif ($component === 'pm_feature') {
            $list_build = $this->_buildStoryList($entity);
          } elseif ($component === 'pm_story') {
            if ($st_build = $this->_buildStoryTask($entity)) {
              $build['story_task'] = $st_build;
            }
            if ($task = $this->getStoryTask($entity)) {
              $lkey = $this->list_keys['pm_task'];
              $list_build = $this->_buildSubTaskList($task);
            }
          } elseif ($component === 'pm_task') {
            $list_build = $this->_buildSubTaskList($entity);
          }
          //
          if ($list_build) {
            $build['section01'] = $this->_buildSection($this->list_labels[$lkey]);
            $build[$lkey] = $list_build;
          }
          break;
        case 'pm_status':
        case 'pm_priority':
          $build['description'] = $this->_buildDescription($entity, FALSE);
          $build['daterange'] = $this->_buildDateStatus($entity);
          break;
        default:
          $build['#markup'] = SlogXt::htmlMessage('....XtPmProject::prepareMatchData', 'warning');
          break;
      }
    }

    $data['build'] = $build;
    return $data;
  }

  /**
   */
  protected function getStatusEntity($entity_id) {
    return $this->getEntity('pm_status', $entity_id);
  }

  /**
   */
  protected function getPriorityEntity($entity_id) {
    return $this->getEntity('pm_priority', $entity_id);
  }

  /**
   */
  protected function getEntity($component, $entity_id) {
    $entities = $this->getEntities($component);
    return ($entities[$entity_id] ?? FALSE);
  }

  /**
   */
  protected function getEntities($component) {
    return SlogXtPm::getComponentEntities($component, $this->project_id);
  }

  /**
   */
  protected function hasEntities($component) {
    $entities = $this->getEntities($component);
    return !empty($entities);
  }

  /**
   */
  protected function getCompIdSpan($cid = 0) {
    if ($cid) {
      $cls = '"xtsi-id"';
      return "<span class=$cls>$cid::&nbsp;</span>";
    }
    return '';
  }

  /**
   */
  protected function _buildSection($title, $cid = 0) {
    $id_span = $this->getCompIdSpan($cid);
    $title = "{$id_span}{$title}";
    return [
        '#prefix' => '<div class="xtpm-section">',
        '#suffix' => '</div>',
        '#markup' => $title,
    ];
  }

  /**
   */
  protected function _buildEmbededPart($title, $content, $cls) {
    $himage = '<div class="xtsi-embed-header-img"></div>';
    $hlink = '<div class="xtsi-embed-header-link"></div>';
    $cls = '"' . "xtsi-embed-header  $cls" . '"';
    $header = "<div class=$cls>$himage $hlink $title</div>";
    $cclass = 'class="xtsi-embed-content"';
    $nclass = 'class="xtsi-embed-node"';
    $build = [
        '#prefix' => "<div $nclass>$header<div $cclass>",
        '#suffix' => '</div></div>',
    ];
    if (is_string($content)) {
      $build['#markup'] = $content;
    } else {
      $build['content'] = $content;
    }

    return $build;
  }

  /**
   */
  protected function getKeyedLine($key, $text, $marker = '- ') {
    $cls = 'class="xtpm-keyed"';
    return "<div $cls>{$marker}{$key}: <span>$text</span></div>";
  }

  /**
   */
  protected function _getGroupLine() {
    $key = t('Group');
    $label = $this->role->label();
    return $this->getKeyedLine($key, $label, '');
  }

  /**
   */
  protected function _buildDescription($entity, $add_group = TRUE) {
    $title = (string) t('Description');
    $description = (string) $entity->get('description')->value;
    $description = SlogXtsi::removeMoreLinkData($description);
    $description = str_replace("\n", '<br />', $description);
    if ($add_group) {
      $group_line = $this->_getGroupLine();
    }
    $content = "$group_line $description";
    return $this->_buildEmbededPart($title, $content, 'description');
  }

  /**
   * {@inheritdoc}
   */
  public static function trustedCallbacks() {
    return ['preRenderView'];
  }

  public static function preRenderView($build) {
    list($node_type, $field_name) = SlogXt::getDescriptionNodeKeys();
    if ($field_name && !empty($build[$field_name])) {
      $element = & $build[$field_name][0];
      $element['#format'] = SlogXt::xtitemsFormatId();
    }

    return $build;
  }

  /**
   */
  protected function _prepareViewData($entity, $clabel = '') {
    static $renderer = NULL;
    if (!$renderer) {
      $renderer = \Drupal::service('renderer');
    }

    //
    $header = $entity->label();
    if ($clabel) {
      $header = "$clabel: $header";
    }

    list($node_type, $field_name) = SlogXt::getDescriptionNodeKeys();
    $node = Node::create([
                'type' => $node_type,
                'title' => '',
                'status' => 1,
    ]);
    $description = (string) $entity->get('description')->value;
    $node->set($field_name, $description);
    $node_preview_controller = NodePreviewController::create(\Drupal::getContainer());
    $element = $node_preview_controller->view($node);
    $element['#pre_render'][] = [static::class, 'preRenderView'];
    $element['#prefix'] = '<div class="xtpm-view">';
    $element['#suffix'] = '</div>';

    //
    $title = SlogXt::htmlPreviewTitle($header);
    $cls = '"xt-dialog-view preview"';

    $build = [
        '#prefix' => "<div class=$cls>$title",
        '#suffix' => '</div>',
        'element' => $element,
    ];
    return $build;
  }

  /**
   */
  protected function _buildStatusPriority($entity) {
    $title = (string) t('Status/Priority');
    $none = (string) t('none');
    // pm_status
    $status_id = (integer) $entity->get('pm_status')->target_id;
    $status = $this->getStatusEntity($status_id);
    if ($status) {
      $sbuild = $this->_prepareViewData($status, t('Status'));
    } else {
      $sbuild = ['#markup' => $this->getKeyedLine(t('Status'), $none)];
    }
    // pm_priority
    $priority_id = (integer) $entity->get('pm_priority')->target_id;
    $priority = $this->getPriorityEntity($priority_id);
    if ($priority) {
      $pbuild = $this->_prepareViewData($priority, t('Priority'));
    } else {
      $pbuild = ['#markup' => $this->getKeyedLine(t('Priority'), $none)];
    }
    if ($status && !$priority) {
      $content = [
          'priority' => $pbuild,
          'status' => $sbuild,
      ];
    } else {
      $content = [
          'status' => $sbuild,
          'priority' => $pbuild,
      ];
    }

    $slabel = $status ? $status->label->value : $none;
    $plabel = $priority ? $priority->label->value : $none;
    $title = "$title: $slabel, $plabel";
    return $this->_buildEmbededPart($title, $content, 'status');
  }

  /**
   */
  protected function _formatDate($date, $is_storage_format = FALSE) {
    static $formatter = NULL;
    if (!$formatter) {
      $formatter = \Drupal::service('date.formatter');
    }
    $timestamp = (integer) $date;
    if ($is_storage_format && !empty($date)) {
      $date_time = new DrupalDateTime($date);
      $timestamp = $date_time->getTimestamp();
    }
    $formatted = $timestamp ? $formatter->format($timestamp, 'xt_date_only') : t('none');
    return (string) $formatted;
  }

  /**
   */
  protected function _buildDateStatus($entity) {
    $title = (string) t('Color/Dates');
    $color = $entity->get('color')->value;
    $created = $this->_formatDate($entity->get('created')->value);
    $changed = $this->_formatDate($entity->get('changed')->value);
    $parts = [
        $this->getKeyedLine(t('Color'), $color),
        $this->getKeyedLine(t('Created'), $created),
        $this->getKeyedLine(t('Last updated'), $changed),
    ];
    $content = implode("\n", $parts);
    return $this->_buildEmbededPart($title, $content, 'daterange');
  }

  /**
   */
  protected function _buildDateRange($entity) {
    $title = (string) t('Dates');
    //
    $pmstart = $this->_formatDate($entity->get('pm_date')->value, TRUE);
    $pmend = $this->_formatDate($entity->get('pm_date')->end_value, TRUE);
    $created = $this->_formatDate($entity->get('created')->value);
    $changed = $this->_formatDate($entity->get('changed')->value);
    $parts = [
        $this->getKeyedLine(t('Start date'), $pmstart),
        $this->getKeyedLine(t('End date'), $pmend),
        $this->getKeyedLine(t('Created'), $created),
        $this->getKeyedLine(t('Last updated'), $changed),
    ];

    $title = "$title: $pmstart - $pmend";
    $content = implode("\n", $parts);
    return $this->_buildEmbededPart($title, $content, 'daterange');
  }

  /**
   */
  protected function _buildComponents($component) {
    $build = [];
    $entities = $this->getEntities($component);
    foreach ($entities as $id => $entity) {
      $build["{$component}.{$id}"] = $this->_buildComponent($entity);
    }

    return $build;
  }

  /**
   */
  protected function _buildComponent($entity, $pre_title = FALSE) {
    $title = (string) $entity->label();
    $entity_id = $entity->id();
    if (!empty($pre_title)) {
      $title = "$pre_title: $title";
    }
    $id_span = $this->getCompIdSpan($entity_id);
    $title = "{$id_span}{$title}";
    //
    $description = (string) $entity->get('description')->value;
    $parts = explode('XtsiMoreLink:sid|id=', $description);
    $sid = (integer) ($parts[1] ?? 0);
    if (count($parts) > 1 && $sid > 0) {
      $description = str_replace("['[XtsiMoreLink:sid|id=", "['[XtsiContext:sid|id=", $description);
    }
    $build = $this->_buildEmbededPart($title, $description, 'description');

    list($node_type, $field_name) = SlogXt::getDescriptionNodeKeys();
    $node = Node::create([
                'type' => $node_type,
                'title' => '',
                'status' => 1,
    ]);
    $node->set($field_name, $description);
    $node_preview_controller = NodePreviewController::create(\Drupal::getContainer());
    $element = $node_preview_controller->view($node);
    $element['#pre_render'][] = [static::class, 'preRenderView'];
    $element['#prefix'] = '<div class="xtpm-view">';
    $element['#suffix'] = '</div>';
    $build['element'] = $element;

    //
    if ($entity->hasField('pm_status')) {
      $none = t('none');
      $status_id = (integer) $entity->get('pm_status')->target_id;
      $status = $this->getStatusEntity($status_id);
      $priority_id = (integer) $entity->get('pm_priority')->target_id;
      $priority = $this->getPriorityEntity($priority_id);
      $lstatus = $status ? $status->label() : $none;
      $lpriority = $priority ? $priority->label() : $none;
      $build['pm_status']['#markup'] = $this->getKeyedLine(t('Status/Priority'), "$lstatus / $lpriority");
    }

    //
    if ($entity->hasField('pm_date')) {
      $pmstart = $this->_formatDate($entity->get('pm_date')->value, TRUE);
      $pmend = $this->_formatDate($entity->get('pm_date')->end_value, TRUE);
      $created = $this->_formatDate($entity->get('created')->value);
      $build['pm_date']['#markup'] = $this->getKeyedLine(t('Created/Start-End'), "$created / $pmstart - $pmend");
    } else {
      $created = $this->_formatDate($entity->get('created')->value);
      $build['pm_date']['#markup'] = $this->getKeyedLine(t('Created'), $created);
    }

    //
    $list_ids = SlogXtPm::getListIdsByEntity($entity);
    if (!empty($list_ids)) {
      $component = $entity->getEntityTypeId();
      $list_key = SlogXtPm::getTargetListKey($component);
      $list_labels = SlogXtPm::getComponentListLabels();
      $comp_label = $list_labels[$list_key];
      $num = t('Num=%num', ['%num' => count($list_ids)]);
      $ids = t('Ids=%ids', ['%ids' => implode(';', $list_ids)]);
      $build['comp_list']['#markup'] = $this->getKeyedLine($comp_label, "$num, $ids");
    }

    unset($build['#markup']);
    return $build;
  }

  /**
   */
  protected function _buildComponentList($component, $base_entity) {
    $build = [];
    $entities = SlogXtPm::getListEntities($component, $base_entity);
    if (!empty($entities)) {
      foreach ($entities as $id => $entity) {
        $build["{$component}.{$id}"] = $this->_buildComponent($entity);
      }
    }

    return $build;
  }

  /**
   */
  protected function _buildBoardColumnList($base_entity) {
    $build = [];
    $lists = [
        'pm_task' => [],
        'pm_story' => [],
        'pm_feature' => [],
        'pm_epic' => [],
    ];
    $component = 'pm_board_column';
    $entities = SlogXtPm::getListEntities($component, $base_entity);
    if (!empty($entities)) {
      foreach ($entities as $id => $entity) {
        $bundle = (string) $entity->bundle->target_id;
        if (isset($lists[$bundle])) {
          $lists[$bundle][] = $entity;
        }
      }
    }

    $lists = array_filter($lists);
    foreach ($lists as $comp => $list) {
      $section = [];

      foreach ($list as $entity) {
        $items = $entity->hasField($comp) ? $entity->get($comp) : FALSE;
        if ($items) {
          $comp_entities = SlogXtPm::getComponentEntities($comp, $this->project_id);
          foreach ($items as $item) {
            $comp_entity_id = (integer) $item->target_id;
            $comp_entity = $comp_entities[$comp_entity_id] ?? FALSE;
            if ($comp_entity) {
              $section["{$comp}.{$comp_entity_id}"] = $this->_buildComponent($comp_entity);
            }
          }
        }
      }

      if (!empty($section)) {
        $build[$comp] = [
            'title' => $this->comp_labels[$comp],
            'content' => $section,
        ];
      }
    }

    return $build;
  }

  /**
   */
  protected function _buildFeatureList($base_entity) {
    return $this->_buildComponentList('pm_feature', $base_entity);
  }

  /**
   */
  protected function _buildStoryList($base_entity) {
    return $this->_buildComponentList('pm_story', $base_entity);
  }

  /**
   */
  protected function _buildSubTaskList($base_entity) {
    return $this->_buildComponentList('pm_sub_task', $base_entity);
  }

  /**
   */
  protected function getStoryTask($entity) {
    $task_id = (integer) $entity->get('pm_task')->target_id;
    if ($task_id) {
      $entities = $this->getEntities('pm_task');
      return ($entities[$task_id] ?? FALSE);
    }
    return FALSE;
  }

  /**
   */
  protected function _buildStoryTask($entity) {
    if ($task = $this->getStoryTask($entity)) {
      $pre_title = (string) $this->comp_labels['pm_task'];
      return $this->_buildComponent($task, $pre_title);
    }

    return FALSE;
  }

  /**
   */
  protected function getReplaceString($data) {
    if (!empty($data['build'])) {
      return (string) $this->getRenderer()->render($data['build']);
    }
    return '';
  }
}
