<?php

/**
 * @file
 * Definition of Drupal\sxt_pm\Plugin\slogxt\edit\role\RoleProjectEdit.
 */

namespace Drupal\sxt_pm\Plugin\slogxt\edit\role;

use Drupal\sxt_slogitem\SlogXtsi;
use Drupal\slogxt\SlogXt;
use Drupal\sxt_group\SxtGroup;
use Drupal\slogxt\XtUserData;
use Drupal\pm_project\Entity\PmProject;
use Drupal\slogxt\Plugin\XtPluginEditBase;

/**
 * @SlogxtEdit(
 *   id = "xtpm_roleproject_edit",
 *   bundle = "rolegroup",
 *   title = @Translation("Project"),
 *   description = @Translation("Administer project components."),
 *   route_name = "sxt_pm.roleproject.edit",
 *   weight = 30
 * )
 */
class RoleProjectEdit extends XtPluginEditBase {

  protected $role = FALSE;
  protected $role_id = FALSE;
  protected $project_id = FALSE;
  protected $project = FALSE;

  /**
   */
  public function getDescription() {
    if (!$this->project_id) {
      return t('Create project for group %role.', ['%role' => $this->role->label()]);
    }
    return t('Administer project components.');
  }

  /**
   */
  public function getActionData($skip_access = FALSE) {
    $data = parent::getActionData();
    if ($this->project_id) {
      $pid = $this->project_id;
      $rid = $this->role_id;
      $data['liDetails'] = "['[XtPmProject:pm_project|id=$pid|role=$rid]']";
      //
      if ($this->project) {
        $description = (string) $this->project->get('description')->value;
        $parsed = SlogXtsi::parseMoreLinkData($description, 'XtsiMoreLink', ['sid']);
        $parsed = $parsed ? reset($parsed) : FALSE;
        if ($parsed && $parsed['type_valid']) {
          $data['liButton2'] = SlogXt::htmlContentLiButton($parsed['id'], TRUE);
        }
      }
    }
    return $data;
  }

  /**
   */
  public function access() {
    $this->role = $role = XtUserData::getDefaultRole(\Drupal::currentUser(), TRUE);
    $this->role_id = $role->id();
    $this->project_id = (integer) $role->get('sxt_pm_project');
    $this->project = $this->project_id ? PmProject::load($this->project_id) : FALSE;
    if ($this->project) {
      return TRUE;
    }
    
    return SxtGroup::hasPermission('admin sxtrole-project');
  }
}
