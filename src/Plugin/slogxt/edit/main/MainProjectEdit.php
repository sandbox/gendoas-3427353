<?php

/**
 * @file
 * Definition of Drupal\sxt_pm\Plugin\slogxt\edit\main\MainProjectEdit
 */

namespace Drupal\sxt_pm\Plugin\slogxt\edit\main;

use Drupal\slogxt\SlogXt;
use Drupal\slogxt\Plugin\XtPluginEditBase;

/**
 * @SlogxtEdit(
 *   id = "xtpm_maincomponent_edit",
 *   bundle = "main",
 *   title = @Translation("Project Management"),
 *   description = @Translation("Administer the base components for the project management."),
 *   route_name = "sxt_pm.mainproject.edit",
 *   weight = 40
 * )
 */
class MainProjectEdit extends XtPluginEditBase {

  protected function getResolveBaseCommand() {
    return FALSE;
  }

  public function access() {
    if (SlogXt::isSuperUser($this->account)) {
      return TRUE;
    }

    return \Drupal::currentUser()->hasPermission('administer xtpm components');
  }

}
