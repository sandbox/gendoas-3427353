<?php

/**
 * @file
 * Contains \Drupal\sxt_pm\Routing\XtpmRoutes.
 */

namespace Drupal\sxt_pm\Routing;

use Drupal\slogtx\SlogTx;
use Drupal\slogxt\SlogXt;
use Symfony\Component\Routing\Route;
use Drupal\slogxt\Routing\SlogxtRoutesBase;

/**
 * Defines a route subscriber to register a url for serving image styles.
 */
class XtpmRoutes extends SlogxtRoutesBase {

  /**
   * Returns an array of route objects.
   *
   * @return \Symfony\Component\Routing\Route[]
   *   An array of route objects.
   */
  public function routes() {
    $basePath = trim(SlogXt::getBasePath('sxt_pm'), '/');
    $baseAjaxPath = trim(SlogXt::getBaseAjaxPath('sxt_pm'), '/');
    $xtsiBaseViewHandle = '\Drupal\sxt_pm\Controller\XtsiBaseViewController::handle';
    $routes = [];

    // this option will be evaluated by \Drupal\sxt_pm\Theme\XtsiNegotiator,
    // i.e. each path with this option uses sjqlout theme.
    $jqlout_true_option = [SlogXt::SJQLOUT_THEME_ROUTE => TRUE];
    // prevent big pipe for some routes
    $no_big_pipe_option = ['_no_big_pipe' => TRUE];

    $raw_data = $this->_rawAjaxAdminPM($baseAjaxPath);

    $defaults = [
        'requirements' => ['_permission' => 'access content'],
        'options' => [],
        'host' => '',
        'schemes' => [],
        'methods' => [],
        'condition' => null,
    ];

    foreach ($raw_data as $key => $data) {
      $data += $defaults;
      $routes["sxt_pm.$key"] = new Route(
              $data['path'], $data['defaults'], $data['requirements'], $data['options'], $data['host'], $data['schemes'], $data['methods'], $data['condition']
      );
    }

    return $routes;
  }

  private function _rawAjaxAdminPM($baseAjaxPath) {
    $controllerPath = '\Drupal\sxt_pm\Handler\admin';
    $base_entity_id = '{base_entity_id}';
    $next_entity_id = '{next_entity_id}';
    $brPath = $baseAjaxPath . '/rolepm';
    $bmPath = $baseAjaxPath . '/mainpm';
    $raw_data = [
        'roleproject.edit' => [
            'path' => "/$brPath/pmedit/$base_entity_id/{project_id}/{component}/{pm_type}/{action}/{entity_id}",
            'defaults' => [
                '_controller' => "{$controllerPath}\RoleProjectEditController::getContentResult",
            ],
            'requirements' => ['_access' => 'TRUE'],
        ],
        'mainproject.edit' => [
            'path' => "/$bmPath/{component}/{entity_id}/{pm_type}/{action}",
            'defaults' => [
                '_controller' => "{$controllerPath}\MainProjectEditController::getContentResult",
            ],
            'requirements' => ['_access' => 'TRUE'],
        ],
        'entity_autocomplete' => [
            'path' => "/$bmPath/$base_entity_id/$next_entity_id/{target_type}/{selection_handler}/{selection_settings_key}",
            'defaults' => [
                '_controller' => "{$controllerPath}\XtPmAutocompleteController::handleAutocomplete",
            ],
            'requirements' => ['_access' => 'TRUE'],
        ],
    ];
    
    return $raw_data;
  }
}
