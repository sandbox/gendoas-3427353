<?php

/**
 * @file
 * Contains \Drupal\sxt_pm\XtPmUserRoleData
 */

namespace Drupal\sxt_pm;

use Drupal\pm_project\Entity\PmProject;
use Drupal\user\Entity\Role;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;

/**
 */
class XtPmUserRoleData {

  /**
   * Add fields to config_export for entity user_role.
   * 
   * @see data for @ConfigEntityType annotation in Role
   * 
   * @param array $entity_types
   *  Array of Drupal\Core\Entity\EntityTypeInterface
   */
  public static function entityTypeBuild(array &$entity_types) {
    if (isset($entity_types['user_role'])) {
      $user_role_type = $entity_types['user_role'];
      $config_export = $user_role_type->get('config_export');
      $config_export[] = 'sxt_pm_project';
      $user_role_type->set('config_export', $config_export);
    }
  }

  /**
   * Add form fields for sxt_pm module.
   * 
   * @param array $form
   * @param FormStateInterface $form_state
   */
  public static function formAlter(&$form, FormStateInterface $form_state) {
    $role = $form_state->getFormObject()->getEntity();
    if (!$role->isNew() && isset($form['slogxtdata']['slogxt_is_xtrole'])) {
      $description = t('No project is created for the group.');
      $project_id = (integer) $role->get('sxt_pm_project');
      if ($project_id && ($project = PmProject::load($project_id))) {
        $args = [
            '%type' => $project->bundle(),
            '%name' => $project->label(),
        ];
        $description = t('Created project: %name (type=%type)', $args);
      }
      //            
      $form['slogxtdata']['sxt_pm_project'] = [
          '#type' => 'textfield',
          '#title' => t('XT-Role Project'),
          '#description' => $description,
          '#default_value' => $project_id,
          '#size' => 10,
          '#disabled' => TRUE,
      ];
    }
  }

  /**
   */
  public static function formAdminRolesAlter(&$form, FormStateInterface $form_state) {
    $entities = &$form['entities'];
    $header = $entities['#header'];

    $new_header = [];
    foreach ($header as $key => $head) {
      $new_header[$key] = $head;
      if ($key === 'weight') {
        $new_header['xt_project'] = t('Project');
      }
    }
    $entities['#header'] = $new_header;
    
    foreach (Element::children($entities) as $role_id) {
      $role_element = $entities[$role_id];
      $role = Role::load($role_id);
      $project_id = (integer) $role->get('sxt_pm_project');
      $sxt_pm_project = $project_id ? $project_id : '-';

      $new_element = [];
      $field_names = Element::children($role_element);
      foreach ($field_names as $field_name) {
        $field = $role_element[$field_name];
        $new_element[$field_name] = $field;
        if ($field_name === 'weight') {
          $new_element['xt_project'] = ['#markup' => $sxt_pm_project];
        }
      }
      $entities[$role_id] = $new_element;
    }
  }
}
